

var attrParam = window.location;
attrGetParam = new URL(attrParam);
attrFilter = attrGetParam.searchParams.get("filter_hersteller");

if(attrFilter == "brother"){
    jQuery('.sidebar-main #pwb_list_widget-8 .pwb-columns:nth-child(1)').addClass('active');
}else if(attrFilter == "canon"){
    jQuery('.sidebar-main #pwb_list_widget-8 .pwb-columns:nth-child(2)').addClass('active');
}else if(attrFilter == "epson"){
    jQuery('.sidebar-main #pwb_list_widget-8 .pwb-columns:nth-child(3)').addClass('active');
}else if(attrFilter == "hewlett-packard"){
    jQuery('.sidebar-main #pwb_list_widget-8 .pwb-columns:nth-child(4)').addClass('active');
}else if(attrFilter == "lexmark"){
    jQuery('.sidebar-main #pwb_list_widget-8 .pwb-columns:nth-child(5)').addClass('active');
}else if(attrFilter == "samsung"){
    jQuery('.sidebar-main #pwb_list_widget-8 .pwb-columns:nth-child(6)').addClass('active');
}else {
    jQuery('.sidebar-main #pwb_list_widget-8 .pwb-columns').removeClass('active');
}




jQuery('.sidebar-main #woocommerce_layered_nav-3 select option').click(function(){
    setTimeout(function(){
    jQuery('#woocommerce_layered_nav-3 .woocommerce-widget-layered-nav-dropdown__submit').trigger( "click" );
    }, 200);
})


var x, i, j, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = jQuery(".sidebar-main #woocommerce_layered_nav-1 .custom-select");
for (i = 0; i < x.length; i++) {
    selElmnt = x[i].getElementsByTagName("select")[0];
    /*for each element, create a new DIV that will act as the selected item:*/
    a = document.createElement("DIV");
    a.setAttribute("class", "select-selected");
    a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
    x[i].appendChild(a);
    /*for each element, create a new DIV that will contain the option list:*/
    b = document.createElement("DIV");
    b.setAttribute("class", "select-items select-hide");
    for (j = 1; j < selElmnt.length; j++) {
        /*for each option in the original select element,
         create a new DIV that will act as an option item:*/
        c = document.createElement("DIV");
        c.innerHTML = selElmnt.options[j].innerHTML;
        c.addEventListener("click", function(e) {
            /*when an item is clicked, update the original select box,
             and the selected item:*/
            var y, i, k, s, h;
            s = this.parentNode.parentNode.getElementsByTagName("select")[0];
            h = this.parentNode.previousSibling;
            for (i = 0; i < s.length; i++) {
                if (s.options[i].innerHTML == this.innerHTML) {
                    s.selectedIndex = i;
                    h.innerHTML = this.innerHTML;
                    y = this.parentNode.getElementsByClassName("same-as-selected");
                    for (k = 0; k < y.length; k++) {
                        y[k].removeAttribute("class");
                    }
                    this.setAttribute("class", "same-as-selected");
                    break;
                }
            }
            h.click();
            jQuery('.sidebar-main #woocommerce_layered_nav-1 .custom-select select').change();
        });
        b.appendChild(c);
    }
    x[i].appendChild(b);
    a.addEventListener("click", function(e) {
        /*when the select box is clicked, close any other select boxes,
         and open/close the current select box:*/
        e.stopPropagation();
        closeAllSelect(this);
        this.nextSibling.classList.toggle("select-hide");
        this.classList.toggle("select-arrow-active");
    });
}
function closeAllSelect(elmnt) {
    /*a function that will close all select boxes in the document,
     except the current select box:*/
    var x, y, i, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");
    for (i = 0; i < y.length; i++) {
        if (elmnt == y[i]) {
            arrNo.push(i)
        } else {
            y[i].classList.remove("select-arrow-active");
        }
    }
    for (i = 0; i < x.length; i++) {
        if (arrNo.indexOf(i)) {
            x[i].classList.add("select-hide");
        }
    }
}
/*if the user clicks anywhere outside the select box,
 then close all select boxes:*/
document.addEventListener("click", closeAllSelect);


var x, i, j, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = jQuery(".sidebar-main #woocommerce_layered_nav-3 .custom-select");
for (i = 0; i < x.length; i++) {
    selElmnt = x[i].getElementsByTagName("select")[0];
    /*for each element, create a new DIV that will act as the selected item:*/
    a = document.createElement("DIV");
    a.setAttribute("class", "select-selected");
    a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
    x[i].appendChild(a);
    /*for each element, create a new DIV that will contain the option list:*/
    b = document.createElement("DIV");
    b.setAttribute("class", "select-items select-hide");
    for (j = 1; j < selElmnt.length; j++) {
        /*for each option in the original select element,
         create a new DIV that will act as an option item:*/
        c = document.createElement("DIV");
        c.innerHTML = selElmnt.options[j].innerHTML;
        c.addEventListener("click", function(e) {
            /*when an item is clicked, update the original select box,
             and the selected item:*/
            var y, i, k, s, h;
            s = this.parentNode.parentNode.getElementsByTagName("select")[0];
            h = this.parentNode.previousSibling;
            for (i = 0; i < s.length; i++) {
                if (s.options[i].innerHTML == this.innerHTML) {
                    s.selectedIndex = i;
                    h.innerHTML = this.innerHTML;
                    y = this.parentNode.getElementsByClassName("same-as-selected");
                    for (k = 0; k < y.length; k++) {
                        y[k].removeAttribute("class");
                    }
                    this.setAttribute("class", "same-as-selected");
                    break;
                }
            }
            h.click();
            jQuery('.sidebar-main #woocommerce_layered_nav-3 .custom-select select').change();
        });
        b.appendChild(c);
    }
    x[i].appendChild(b);
    a.addEventListener("click", function(e) {
        /*when the select box is clicked, close any other select boxes,
         and open/close the current select box:*/
        e.stopPropagation();
        closeAllSelect(this);
        this.nextSibling.classList.toggle("select-hide");
        this.classList.toggle("select-arrow-active");
    });
}
function closeAllSelect(elmnt) {
    /*a function that will close all select boxes in the document,
     except the current select box:*/
    var x, y, i, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");
    for (i = 0; i < y.length; i++) {
        if (elmnt == y[i]) {
            arrNo.push(i)
        } else {
            y[i].classList.remove("select-arrow-active");
        }
    }
    for (i = 0; i < x.length; i++) {
        if (arrNo.indexOf(i)) {
            x[i].classList.add("select-hide");
        }
    }
}
/*if the user clicks anywhere outside the select box,
 then close all select boxes:*/
document.addEventListener("click", closeAllSelect);



jQuery('.sidebar-main #woocommerce_layered_nav-1 .select-selected').text('andere Hersteller');

jQuery('.sidebar-main #woocommerce_layered_nav-3 .woocommerce-widget-layered-nav-dropdown select option:nth-child(1)').hide();

//jQuery('.sidebar-main #pwb_list_widget-8').insertBefore(".sidebar-main #woocommerce_layered_nav-1 form.woocommerce-widget-layered-nav-dropdown");


setTimeout(function(){
    jQuery('.sidebar-main #woocommerce_layered_nav-1 .custom-select select').select2({
        minimumInputLength: 2
    });
    jQuery('.woocommerce .select2-container--default .select2-selection--single .select2-selection__rendered').text('Schnellesuche');
    jQuery('#woocommerce_layered_nav-1 .select2-container').insertBefore('#woocommerce_layered_nav-1 #pwb_list_widget-8');


    jQuery('.sidebar-main #woocommerce_layered_nav-3 .woocommerce-widget-layered-nav-dropdown select').select2({
        minimumInputLength: 2
    });
    //jQuery('.sidebar-main #woocommerce_layered_nav-3 select').attr('multiple', 'multiple');
    jQuery('.woocommerce .select2-container--default .select2-selection--single .select2-selection__rendered').text('Schnellesuche');
    jQuery('#woocommerce_layered_nav-3 .select2-container.select2-container--default').insertBefore('#woocommerce_layered_nav-3 form.woocommerce-widget-layered-nav-dropdown');

    var title = jQuery('#woocommerce_layered_nav-3 .custom-select .select-selected').text();

    var divSlect = jQuery('#woocommerce_layered_nav-3 .custom-select .select-hide div');

    divSlect.each(function(){
        var text = jQuery(this).text();
        if(text == title){
            jQuery(this).addClass('active');
        }
    });

    jQuery('#woocommerce_layered_nav-3 .custom-select .select-hide').animate({
        scrollTop: jQuery("#woocommerce_layered_nav-3 .custom-select .select-hide div.active").offset().top - 300
    }, 100);

}, 500);


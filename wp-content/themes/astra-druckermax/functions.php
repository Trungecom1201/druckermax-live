<?php
/**
 * Druckermax Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Druckermax
 * @since 1.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_DRUCKERMAX_VERSION', '1.0' );

/**
 * Enqueue styles
 */


register_sidebar(array(
    'name' => 'Brand',
    'id' => 'brand-content',
    'description' => 'Add widgets here to appear in your Header.',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h2 class="widget-title">',
    'after_title' => '</h2>'
));

function child_enqueue_styles() {

    wp_enqueue_style( 'druckermax-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_DRUCKERMAX_VERSION, 'all' );

}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );


function dateitypen_bearbeiten($mime_types){
    $mime_types['json'] = 'image/json+xml'; //.json hinzufügen
    return $mime_types;
}
add_filter('upload_mimes', 'dateitypen_bearbeiten', 1, 1);


// Add content to astra_header_before()
add_action( 'astra_masthead_content', 'add_content_before_header' );
function add_content_before_header() { ?>
    <?php if ( function_exists( 'aws_get_search_form' ) ) { aws_get_search_form(); } ?>
<?php }

function ws_scripts_and_styles() {
    wp_enqueue_style('custom-style', get_stylesheet_directory_uri() .'/assets/css/custom.css');
    wp_enqueue_script('theme-js', get_stylesheet_directory_uri() .'/assets/js/theme.js', array(), time(), true, true);
}
add_action( 'wp_enqueue_scripts', 'ws_scripts_and_styles' );
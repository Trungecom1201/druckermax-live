<?php

/**
 * Shortcodes
 *
 * @author jj,ap
 */
Class WGM_Shortcodes {

	public static function register() {

		/**
		 * Add hooks
		 */
		add_shortcode( 'woocommerce_de_disclaimer_deadline', array( __CLASS__, 'add_shortcode_disclaimer_deadline' ) );
		add_shortcode( 'woocommerce_de_disclaimer_address_data',
		               array( __CLASS__, 'add_shortcode_disclaimer_address_data' ) );
		
		if ( Woocommerce_German_Market::is_frontend() ) {
			add_shortcode( 'woocommerce_de_check', array( __CLASS__, 'add_shortcode_check' ) );
		}
		
		remove_shortcode( 'woocommerce_pay' );
	}

	/**
	 * Shortcode for the amount of days to withdraw to include in Disclaimer page
	 *
	 * @access      public
	 * @static
	 * @uses        get_option
	 * @return    string days and singular/plural of day
	 */
	public static function add_shortcode_disclaimer_deadline() {

		$option = get_option( WGM_Helper::get_wgm_option( 'widerrufsfrist' ), 14 );
		$days   = absint( $option );
		$string = sprintf(
			_n( '%s day', '%s days', $days, 'woocommerce-german-market' ),
			$days
		);

		return $string;
	}

	/**
	 * withdraw address shortcode for the disclaimer page
	 *
	 * @access      public
	 * @uses        get_option
	 * @static
	 * @return    string withdraw address
	 */
	public static function add_shortcode_disclaimer_address_data() {

		return nl2br( get_option( WGM_Helper::get_wgm_option( 'widerrufsadressdaten' ) ) );
	}

	/**
	 * Shortcode for the second checkout page in the german Version
	 *
	 * @access    public
	 * @static
	 * @return    string template conents
	 */
	public static function add_shortcode_check() {

		ob_start();

		WGM_Template::load_template( 'second-checkout2.php' );

		$tpl = ob_get_contents();
		ob_end_clean();

		return $tpl;
	}
}

?>
<?php

/**
 * Class WGM_Compatibilities
 *
 * German Market Userinterface
 *
 * @author MarketPress
 */
class WGM_Compatibilities {

	/**
	 * @var WGM_Compatibilities
	 * @since v3.1.2
	 */
	private static $instance = null;
	
	/**
	* Subscriptions WGM_Compatibility Variables
	**/
	private $_new_order_create;
    private $_wp_wc_running_invoice_number;
    private $_wp_wc_running_invoice_number_date;

	/**
	* Singletone get_instance
	*
	* @static
	* @return WGM_Compatibilities
	*/
	public static function get_instance() {
		if ( self::$instance == NULL) {
			self::$instance = new WGM_Compatibilities();	
		}
		return self::$instance;
	}

	/**
	* Singletone constructor
	*
	* @access private
	*/
	private function __construct() {

		// Checkout Strings
		$options = array(
			'woocommerce_de_estimate_cart_text',
			'woocommerce_de_avoid_free_items_in_cart_message',
			'woocommerce_de_order_button_text',
			'woocommerce_de_checkbox_text_digital_content',
			'woocommerce_de_checkbox_text_digital_content_notice',
			'woocommerce_de_learn_more_about_shipping_payment_revocation',
			'vat_options_notice',
			'vat_options_non_eu_notice',
			'vat_options_label',
			'gm_small_trading_exemption_notice',
			'gm_small_trading_exemption_notice_extern_products',
			'wgm_default_tax_label',
			'german_market_checkbox_1_tac_pd_rp_text_no_digital',
			'german_market_checkbox_1_tac_pd_rp_text_digital_only_digital',
			'german_market_checkbox_1_tac_pd_rp_text_mix_digital',
			'german_market_checkbox_1_tac_pd_rp_error_text_no_digital',
			'german_market_checkbox_1_tac_pd_rp_error_text_digital_only_digital',
			'german_market_checkbox_1_tac_pd_rp_error_text_mix_digital',
			'woocommerce_de_checkbox_error_text_digital_content',
			'german_market_checkbox_3_shipping_service_provider_text',
			'german_market_checkbox_3_shipping_service_provider_error_text',
			'german_market_checkbox_4_custom_text',
			'german_market_checkbox_4_custom_error_text',
			'gm_checkbox_5_my_account_registration_text',
			'gm_checkbox_5_my_account_registration_error_text',
			'gm_checkbox_6_product_review_text',
			'gm_checkbox_6_product_review_error_text',
			'gm_order_confirmation_mail_subject',
			'gm_order_confirmation_mail_heading',
			'gm_order_confirmation_mail_text',
			'woocommerce_de_show_extra_cost_hint_eu_text',
		);

		$add_ons = WGM_Add_Ons::get_activated_add_ons();

		// Invoice PDF
		if ( isset( $add_ons[ 'woocommerce-invoice-pdf' ] ) ) {
			
			$options[] = 'wp_wc_invoice_pdf_file_name_frontend';
			$options[] = 'wp_wc_invoice_pdf_file_name_backend';
			$options[] = 'wp_wc_invoice_pdf_billing_address_additional_notation';
			$options[] = 'wp_wc_invoice_pdf_invoice_start_subject';
			$options[] = 'wp_wc_invoice_pdf_invoice_start_welcome_text';
			$options[] = 'wp_wc_invoice_pdf_text_after_content';
			$options[] = 'wp_wc_invoice_pdf_page_numbers_text';
			$options[] = 'wp_wc_invoice_pdf_fine_print_custom_content';
			$options[] = 'wp_wc_invoice_pdf_refund_file_name_frontend';
			$options[] = 'wp_wc_invoice_pdf_refund_file_name_backend';
			$options[] = 'wp_wc_invoice_pdf_refund_start_subject_big';
			$options[] = 'wp_wc_invoice_pdf_refund_start_subject_small';
			$options[] = 'wp_wc_invoice_pdf_refund_start_refund_date';
			$options[] = 'wp_wc_invoice_pdf_view_order_button_text';

			$header_columns = get_option( 'wp_wc_invoice_pdf_header_number_of_columns', 1 );
			for ( $i = 1; $i <= $header_columns; $i++ ) {
				$options[] = 'wp_wc_invoice_pdf_header_column_' . $i . '_text';
			}

			$footer_columns = get_option( 'wp_wc_invoice_pdf_footer_number_of_columns', 1 );
			for ( $i = 1; $i <= $footer_columns; $i++ ) {
				$options[] = 'wp_wc_invoice_pdf_footer_column_' . $i . '_text';
			}

		}

		// Invoice Numbers
		if ( isset( $add_ons[ 'woocommerce-running-invoice-number' ] ) ) {
			
			$options[] = 'wp_wc_running_invoice_completed_order_email_subject';
			$options[] = 'wp_wc_running_invoice_completed_order_email_header';
			$options[] = 'wp_wc_running_invoice_email_subject';
			$options[] = 'wp_wc_running_invoice_email_header';
			$options[] = 'wp_wc_running_invoice_email_subject_paid';
			$options[] = 'wp_wc_running_invoice_email_header_paid';
			$options[] = 'wp_wc_running_invoice_email_subject_refunded';
			$options[] = 'wp_wc_running_invoice_email_header_refunded';

			$options[] = 'wp_wc_running_invoice_pdf_file_name_backend';
			$options[] = 'wp_wc_running_invoice_pdf_file_name_frontend';
			$options[] = 'wp_wc_running_invoice_pdf_subject';
			$options[] = 'wp_wc_running_invoice_pdf_date';

			$options[] = 'wp_wc_running_invoice_pdf_file_name_backend_refund';
			$options[] = 'wp_wc_running_invoice_pdf_file_name_frontend_refund';
			$options[] = 'wp_wc_running_invoice_pdf_refund_start_subject_big';
			$options[] = 'wp_wc_running_invoice_pdf_refund_start_subject_small';

		}

		// Return Delivery
		if ( isset( $add_ons[ 'woocommerce-return-delivery-pdf' ] ) ) {
			
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_file_name';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_author';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_title';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_shop_name';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_address';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_shop_small_headline';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_remark';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_reasons';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_footer';

			$options[] = 'woocomerce_wcreapdf_wgm_pdf_file_name_delivery';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_author_delivery';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_title_delivery';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_shop_name_delivery';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_address_delivery';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_shop_small_headline_delivery';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_remark_delivery';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_reasons_delivery';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_footer_delivery';

			$options[] = 'woocomerce_wcreapdf_wgm_view-order-button-text';
		}

		// Due Date
		if ( get_option( 'woocommerce_de_due_date', 'off' ) == 'on' ) {

			add_action( 'admin_init', array( $this, 'due_date' ) );

		}

		/******************************
		// Compatibility with WPML
		******************************/
		
		if ( function_exists( 'icl_register_string' ) && function_exists( 'icl_t' ) && function_exists( 'icl_st_is_registered_string' ) ) {
			
			add_filter( 'woocommerce_find_rates', array( $this, 'translate_woocommerce_find_rates' ), 10 );
			add_filter( 'wgm_translate_tax_label', array( $this, 'translate_tax_label' ) );
			add_filter( 'woocommerce_de_get_deliverytime_label_term', array( $this, 'wpml_translate_delivery_times' ), 10, 2 );

			// Register Strings
			if ( is_admin() ) {
				
				$tax_classes = WC_Tax::get_tax_classes();
				 
				$tax_classes[] = 'standard';

				foreach ( $tax_classes as $tax_class ) {
				 	
				 	$rates = WC_Tax::get_rates_for_tax_class( $tax_class );
				 	foreach ( $rates as $rate ) {
				 		$label = $rate->tax_rate_name;
				 		if ( ! icl_st_is_registered_string( 'German Market: WooCommerce Tax Rate', 'tax rate label: ' . $label ) ) {
	                        icl_register_string( 'German Market: WooCommerce Tax Rate', 'tax rate label: ' . $label, $label );
	                    }
				 	}
				}

			}

			foreach ( $options as $option ) {

				if ( is_admin() ) {
					if ( ! ( icl_st_is_registered_string( 'German Market: Checkout Option', $option ) || icl_st_is_registered_string( 'German Market: Invoice PDF', $option ) || icl_st_is_registered_string( 'German Market: Running Invoice Number', $option ) || icl_st_is_registered_string( 'German Market: Return Delivery Note', $option ) ) ) {
	                    
						if ( str_replace( 'wp_wc_invoice_pdf_', '', $option ) != $option ) {
							icl_register_string( 'German Market: Invoice PDF', $option, get_option( $option ) );

						} else if ( str_replace( 'wp_wc_running_invoice_', '', $option ) != $option ) {
							icl_register_string( 'German Market: Running Invoice Number', $option, get_option( $option ) );

						} else if ( ( str_replace( 'woocomerce_wcreapdf_wgm_', '', $option ) != $option ) || ( str_replace( 'woocommerce_wcreapdf_wgm_', '', $option ) != $option  ) ) {
							icl_register_string( 'German Market: Return Delivery Note', $option, get_option( $option ) );

						} else {
							icl_register_string( 'German Market: Checkout Option', $option, get_option( $option ) );
						}
	                    
	                }

	            }

                if ( ( ! ( is_admin() && isset( $_REQUEST[ 'page' ] ) && $_REQUEST[ 'page' ] == 'german-market' ) ) || ( ! is_admin() ) ) {
                	add_filter( 'option_' . $option, array( $this, 'translate_woocommerce_checkout_options' ), 10, 2 );
                }

			}

		}

		/******************************
		// Compatibility with WPML: WooCommerce Multilingual 
		******************************/

		// Onliy if WooCommerce Multilingual && WPML && GM Invoice PDF ADd-On
		if ( is_admin() && class_exists( 'WCML_Admin_Menus' ) && class_exists( 'SitePress' ) && ( ( get_option( 'wgm_add_on_woocommerce_invoice_pdf', 'off' ) == 'on' ) || ( get_option( 'wgm_add_on_woocommerce_return_delivery_pdf', 'off' ) == 'on' ) ) ) {
			
			add_filter( 'woocommerce_de_ui_options_global', array( $this, 'woocommerece_multilingual_invoice_pdf_admin_download' ) );
			
			if ( get_option( 'wp_wc_invoice_pdf_wpml_admin_download_language', 'switcher' ) == 'switcher' ) {
				
				// add language switcher to order page in backend
				add_action( 'current_screen', array( $this, 'woocommerece_multilingual_add_language_switcher' ) );
			
			} else {

				if ( get_option( 'wgm_add_on_woocommerce_invoice_pdf', 'off' ) == 'on' ) {
					add_action( 'wp_wc_invoice_pdf_start_template', array( $this, 'wpml_invoice_pdf_admin_download_switch_lang' ) );
					add_action( 'wp_wc_invoice_pdf_end_template', array( $this, 'wpml_invoice_pdf_admin_download_reswitch_lang' ) );
				}

				if ( get_option( 'wgm_add_on_woocommerce_return_delivery_pdf', 'off' ) == 'on' ) {
					add_action( 'wcreapdf_pdf_before_create', array( $this, 'wpml_retoure_pdf_admin_download_switch_lang' ), 10, 2 );
					add_action( 'wcreapdf_pdf_after_create', array( $this, 'wpml_retoure_pdf_admin_download_reswitch_lang' ), 10, 2 );
				}

			}
			
		}


		/******************************
		// Compatibility with polylang
		******************************/
		if ( function_exists( 'pll_register_string' ) && function_exists( 'pll__' ) ){

			foreach ( $options as $option ) {
				pll_register_string( $option, get_option( $option ), 'German Market: Checkout Option', true );
				add_filter( 'option_' . $option, array( $this, 'translate_woocommerce_checkout_options_polylang' ), 10, 2 );
			}

			// Delivery Times
			add_filter( 'woocommerce_de_get_deliverytime_string_label_string', array( $this, 'polylang_woocommerce_de_get_deliverytime_string_label_string' ), 10, 2 );
		}
		
		/******************************
		// Compatibility with WooCommerce Composite Products
		******************************/
		if ( class_exists( 'WC_Product_Composite' ) ) {
			add_filter( 'gm_compatibility_is_variable_wgm_template', '__return_false' );
			add_filter( 'gm_cart_template_force_woocommerce_template', '__return_true' );
			add_filter( 'gm_remove_woo_vat_notice_return_original_param', '__return_true' );
		}

		/******************************
		// Compatibility WooCommerce Subscriptions
		******************************/
		if ( function_exists( 'wcs_cart_totals_order_total_html' ) ) {
			add_action( 'german_market_after_frontend_init', array( $this, 'subscriptions' ) );
		}

		if ( class_exists( 'WC_Subscriptions' ) ) {
			add_filter( 'gm_invoice_pdf_email_settings', 					array( $this, 'subscriptions_gm_invoice_pdf_email_settings' ) );
			add_filter( 'gm_invoice_pdf_email_settings_additonal_pdfs', 	array( $this, 'subscriptions_gm_invoice_pdf_email_settings' ) );
			add_filter( 'wcreapdf_allowed_stati', 							array( $this, 'subscriptions_gm_allowed_stati_additional_mals' ) );
			add_filter( 'wp_wc_inovice_pdf_allowed_stati', 					array( $this, 'subscriptions_gm_allowed_stati_additional_mals' ) );
			add_filter( 'wp_wc_inovice_pdf_allowed_stati_additional_mals', 	array( $this, 'subscriptions_gm_allowed_stati_additional_mals' ) );
			add_filter( 'wcreapdf_email_options_after_sectioned',			array( $this, 'subscriptions_gm_retoure_pdf_email_settings' ) );
			add_filter( 'gm_emails_in_add_ons', 							array( $this, 'subscriptions_gm_emails_in_add_ons' ) );

			if ( has_filter( 'wcs_new_order_created' ) ) {
	            add_action( 'woocommerce_new_order', array( $this, 'action_woocommerce_new_order' ), 70, 1 );
	            add_filter( 'wcs_new_order_created', array( $this, 'filter_wcs_new_order_created' ), 70, 1) ;
	        }

	        add_filter( 'woocommerce_countries_inc_tax_or_vat', array( $this, 'dummy_remove_woo_vat_notice' ), 70, 1 );
	        add_filter( 'woocommerce_countries_ex_tax_or_vat', array( $this, 'dummy_remove_woo_vat_notice' ), 70, 1 );

	        // Don't copy invoice number, invoice date or due date form first order to subscription object
			add_filter( 'wcs_renewal_order_meta', array( $this, 'subscriptions_gm_dont_copy_meta' ), 10, 3 );
			
		}

		/******************************
		// Compatibility for plugins with custom email status
		******************************/
		add_filter( 'gm_invoice_pdf_email_settings', 					array( $this, 'custom_email_status_gm_invoice_pdf_email_settings' ) );
		add_filter( 'gm_invoice_pdf_email_settings_additonal_pdfs', 	array( $this, 'custom_email_status_gm_invoice_pdf_email_settings' ) );
		add_filter( 'wcreapdf_email_options_after_sectioned',			array( $this, 'custom_email_status_gm_retoure_pdf_email_settings' ) );
		add_filter( 'wcreapdf_allowed_stati', 							array( $this, 'custom_email_status_gm_allowed_stati_additional_mals' ) );
		add_filter( 'wp_wc_inovice_pdf_allowed_stati', 					array( $this, 'custom_email_status_gm_allowed_stati_additional_mals' ) );
		add_filter( 'wp_wc_inovice_pdf_allowed_stati_additional_mals', 	array( $this, 'custom_email_status_gm_allowed_stati_additional_mals' ) );
		add_filter( 'gm_emails_in_add_ons', 							array( $this, 'custom_email_status_gm_emails_in_add_ons' ) );

		/******************************
		// Theme Compabilities
		******************************/

		$the_theme = wp_get_theme();

		// Theme aurum
		if ( $the_theme->get( 'TextDomain' ) == 'aurum' || $the_theme->get( 'TextDomain' ) == 'annakait' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_support_aurum' ) );
		}

		// Theme avada
		else if ( $the_theme->get( 'TextDomain' ) == 'Avada' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_support_avada' ) );
		}

		// Theme Superba
		else if ( $the_theme->get( 'TextDomain' ) == 'thb_text_domain' || ( $the_theme->get_template() == 'superba' ) ) {
			add_action( 'init', array( $this, 'theme_support_superba' ), 20 );
		}

		// Theme woodance
		else if ( ( ! is_admin() ) && ( $the_theme->get( 'TextDomain' ) == 'woodance' || ( $the_theme->get_template() == 'woodance' ) ) ) {
			add_action( 'wp', array( $this, 'theme_support_woodance' ) );
		}

		// Theme Envision
		else if ( $the_theme->get_template() == 'envision' || $the_theme->get_stylesheet() == 'envision' ) {
			add_action( 'german_market_after_frontend_init', array( $this, 'theme_support_envision' ) );
		}

		// Theme Fluent
		else if ( $the_theme->get_template() == 'fluent' || $the_theme->get_stylesheet() == 'fluent' || $the_theme->get_stylesheet() == 'fluent-child' ) {
			add_action( 'wp', array( $this, 'theme_support_fluent' ) );
		}

		// Theme Woodstroid
		else if ( $the_theme->get_template() == 'woostroid' || $the_theme->get_stylesheet() == 'woostroid' || $the_theme->get_stylesheet() == 'woostroid-child' ) {
			 add_action( 'after_setup_theme', array( $this, 'theme_support_woodstroid' ), 5 );
		}

		// Theme Peddlar
		else if ( $the_theme->get_template() == 'peddlar' || $the_theme->get_stylesheet() == 'peddlar' || $the_theme->get_stylesheet() == 'peddlar-child' ) {
			add_filter( 'wgm_close_a_tag_before_wgm_product_summary_in_loop', '__return_false' );
			add_filter( 'wgm_product_summary_html', array( $this, 'theme_support_peddlar' ), 1, 4 );
		}

		// Theme VG Vegawine
		else if ( $the_theme->get_template() == 'vg-vegawine' || $the_theme->get_stylesheet() == 'vg-vegawine' || $the_theme->get_stylesheet() == 'vg-vegawine-child' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_vegawine' ) );
		}

		// Theme Savoy
		else if ( $the_theme->get_template() == 'savoy' || $the_theme->get_stylesheet() == 'savoy' || $the_theme->get_stylesheet() == 'savoy-child' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_savoy' ) );
		}

		// Theme Kryia
		else if ( $the_theme->get_template() == 'kriya' || $the_theme->get_stylesheet() == 'kriya' || $the_theme->get_stylesheet() == 'kriya-child' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_kriya' ) );
		}

		// Theme Hestia Pro
		else if ( $the_theme->get_template() == 'hestia-pro' || $the_theme->get_stylesheet() == 'hestia-pro' || $the_theme->get_stylesheet() == 'hestia-pro-child' || $the_theme->get_template() == 'hestia' || $the_theme->get_stylesheet() == 'hestia' || $the_theme->get_stylesheet() == 'hestia-child' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_hestia_pro' ), 10, 3 );
		}

		// Theme The7		
		else if ( $the_theme->get_template() == 'dt-the7' || $the_theme->get_stylesheet() == 'dt-the7' || $the_theme->get_stylesheet() == 'dt-the7-child' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_the7' ) );
		
		// Theme Sober
		} else if ( $the_theme->get_template() == 'sober' || $the_theme->get_stylesheet() == 'sober-the7' || $the_theme->get_stylesheet() == 'sober-child' ) {
			add_action( 'german_market_after_frontend_init', array( $this, 'theme_support_sober' ) );
		
		// Theme XStore
		} else if ( $the_theme->get_template() == 'xstore' || $the_theme->get_stylesheet() == 'xstore' || $the_theme->get_stylesheet() == 'xstore-child' ) {
			add_action( 'wp', array( $this, 'theme_support_xstore' ), 61 );
		
		// Theme Worldmart
		} else if ( $the_theme->get_template() == 'worldmart' || $the_theme->get_stylesheet() == 'worldmart' || $the_theme->get_stylesheet() == 'worldmart-child' ) {
			add_action( 'wp', array( $this, 'theme_support_worldmart' ), 10 );
		
		// Muenchen
		} else if ( $the_theme->get_template() == 'muenchen' || $the_theme->get_stylesheet() == 'muenchen' || $the_theme->get_stylesheet() == 'muenchen-child' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_support_cart_template_remove_taxes_in_subototal' ) );

		// Peony
		} else if ( $the_theme->get_template() == 'peony' || $the_theme->get_stylesheet() == 'peony' || $the_theme->get_stylesheet() == 'peony-child' ) {
			add_action( 'wp', array( $this, 'theme_support_peony' ), 10 );

		// Elessi
		} else if ( $the_theme->get_template() == 'elessi-theme' || $the_theme->get_stylesheet() == 'elessi-theme' || $the_theme->get_stylesheet() == 'elessi-theme-child' ) {
			add_action( 'init', array( $this, 'theme_support_elessi' ), 11 );
	
		// Ronneby
		} else if ( $the_theme->get_template() == 'ronneby' || $the_theme->get_stylesheet() == 'ronneby' || $the_theme->get_stylesheet() == 'ronneby-child' ) {
			add_action( 'init', array( $this, 'theme_support_ronneby' ), 11 );
		
		// Zass
		} else if ( $the_theme->get_template() == 'zass' || $the_theme->get_stylesheet() == 'zass' || $the_theme->get_stylesheet() == 'zass-child' ) {
			add_action( 'german_market_after_frontend_init', array( $this, 'theme_support_zass' ), 11 );
		
		// Kalium
		} else if ( $the_theme->get_template() == 'kalium' || $the_theme->get_stylesheet() == 'kalium' || $the_theme->get_stylesheet() == 'kalium-child' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_support_kalium' ), 11 );
		
		// handsome-shop
		} else if ( $the_theme->get_template() == 'handmade-shop' || $the_theme->get_stylesheet() == 'handmade-shop' || $the_theme->get_stylesheet() == 'handmade-shop-child' ) {
			add_action( 'init', array( $this, 'theme_support_handmade_shop' ), 11 );
		
		// iustore
		} else if ( str_replace( 'iustore', '', $the_theme->get_template() ) != $the_theme->get_template() || str_replace( 'iustore', '', $the_theme->get_stylesheet() ) != $the_theme->get_stylesheet() || str_replace( 'iustore-child', '', $the_theme->get_stylesheet() ) != $the_theme->get_stylesheet() || $the_theme->get( 'TextDomain' ) == 'iustore' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_support_iustore' ) );
		
		// amely
		} else if ( $the_theme->get_template() == 'amely' || $the_theme->get_stylesheet() == 'amely' || $the_theme->get_stylesheet() == 'amely-child' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_support_amely' ), 11 );
		
		// OceanWP
		} else if ( $the_theme->get_template() == 'oceanwp' || $the_theme->get_stylesheet() == 'oceanwp' || $the_theme->get_stylesheet() == 'oceanwp-child' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_support_oceanwp' ), 11 );

		// Astra
		} else if ( $the_theme->get_template() == 'astra' || $the_theme->get_stylesheet() == 'astra' || $the_theme->get_stylesheet() == 'astra-child' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_support_astra' ), 11 );

		// Ecode
		} else if ( $the_theme->get_template() == 'ecode' || $the_theme->get_stylesheet() == 'ecode' || $the_theme->get_stylesheet() == 'ecode-child' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_support_ecode' ), 11 );

		// Alishop
		} else if ( $the_theme->get_template() == 'alishop' || $the_theme->get_stylesheet() == 'alishop' || $the_theme->get_stylesheet() == 'alishop-child' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_support_alishop' ), 11 );

		// Bazien
		} else if ( $the_theme->get_template() == 'bazien' || $the_theme->get_stylesheet() == 'bazien' || $the_theme->get_stylesheet() == 'bazien-child' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_support_bazien' ), 11 );

		// Technigs
		} else if ( $the_theme->get_template() == 'technics' || $the_theme->get_stylesheet() == 'technics' || $the_theme->get_stylesheet() == 'technics-child' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_support_technics' ), 11 );

		// Flatsome Page Builder
		} else if ( $the_theme->get_template() == 'flatsome' || $the_theme->get_stylesheet() == 'flatsome' || $the_theme->get_stylesheet() == 'flatsome-child' ) {
			add_action( 'woocommerce_after_template_part', array( $this, 'theme_flatsome_price_data' ), 10, 4 );

		// Supro
		} else if ( $the_theme->get_template() == 'supro' || $the_theme->get_stylesheet() == 'supro' || $the_theme->get_stylesheet() == 'supro-child' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_support_supro' ), 10 );

		// Erado
		} else if ( $the_theme->get_template() == 'erado' || $the_theme->get_stylesheet() == 'erado' || $the_theme->get_stylesheet() == 'erado-child' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_support_erado' ), 10 );

		// Ordo
		} else if ( $the_theme->get( 'TextDomain' ) == 'ordo' || $the_theme->get_template() == 'ordo' || $the_theme->get_stylesheet() == 'ordo' || $the_theme->get_stylesheet() == 'ordo-child' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_support_ordo' ), 10 );

		// Variegated
		} else if ( $the_theme->get_template() == 'variegated' || $the_theme->get_stylesheet() == 'variegated' || $the_theme->get_stylesheet() == 'variegated-child' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_support_variegated' ), 10 );
			add_action( 'wp_head', array( $this, 'theme_support_variegated_css' ) );

		// Adorn
		} else if ( $the_theme->get_template() == 'adorn' || $the_theme->get_stylesheet() == 'adorn' || $the_theme->get_stylesheet() == 'adorn-child' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_support_adorn' ), 10 );

		// Hypermarket
		} else if ( $the_theme->get_template() == 'hypermarket' || $the_theme->get_stylesheet() == 'hypermarket' || $the_theme->get_stylesheet() == 'hypermarket-child' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_support_hypermarket' ), 10 );

		// Planetshine Polaris
		} else if ( $the_theme->get_template() == 'planetshine-polaris' || $the_theme->get_stylesheet() == 'planetshine-polaris' || $the_theme->get_stylesheet() == 'planetshine-polaris-child' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_support_planetshine_polaris' ), 10 );

		// Electro
		} else if ( $the_theme->get_template() == 'electro' || $the_theme->get_stylesheet() == 'electro' || $the_theme->get_stylesheet() == 'electro-child' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_support_electro' ), 11 );

		// Justshop
		} else if ( $the_theme->get_template() == 'justshop' || $the_theme->get_stylesheet() == 'justshop' || $the_theme->get_stylesheet() == 'justshop-child' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_support_jutshop' ), 11 );

		// Page Builder Framework
		} else if ( $the_theme->get( 'TextDomain' ) == 'page-builder-framework' || $the_theme->get_template() == 'page-builder-framework' || $the_theme->get_stylesheet() == 'page-builder-framework' || $the_theme->get_stylesheet() == 'page-builder-framework-child' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_support_page_builder_framework' ), 11 );
		
		// DFD Native
		}  else if ( $the_theme->get( 'TextDomain' ) == 'dfd-native' || $the_theme->get_template() == 'dfd-native' || $the_theme->get_stylesheet() == 'dfd-native' || $the_theme->get_stylesheet() == 'dfd-native-child' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_support_dfd_native' ), 11 );
		
		// Ciloe
		} else if ( $the_theme->get_template() == 'ciloe' || $the_theme->get_stylesheet() == 'ciloe' || $the_theme->get_stylesheet() == 'ciloe-child' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_support_ciloe' ), 11 );

		}

		/******************************
		// WPBakeryVisualComposer Page Builder
		******************************/
		if ( class_exists( 'Vc_Manager' ) ) {
			add_filter( 'woocommerce_get_price_html', array( $this, 'wp_bakery_woocommerce_get_price_html' ), 10, 2 );
		}

		/******************************
		// Divi Page Builder
		******************************/
		add_action ( 'wp_enqueue_scripts', array( $this, 'divi_page_builder' ), 20 );

		/******************************
		// Plugins YITH WooCommerce Best Sellers & YITH WooCommerce Wishlist
		******************************/
		if ( defined( 'YITH_WCBSL_VERSION' ) || defined( 'YITH_WCWL' ) ) {
			add_filter( 'woocommerce_product_title', array( $this, 'plugins_yith_wl_bs' ), 10, 2 );
		}

		/******************************
		// Plugin Elementor
		******************************/
		if ( defined( 'ELEMENTOR_VERSION' ) ) {
			add_action( 'wgm_email_before_get_email_de_footer', array( $this, 'plugin_elementor_remove_filters' ) );
			add_action( 'wgm_email_ater_get_email_de_footer', 	array( $this, 'plugin_elementor_add_filters_again' ) );
			add_action( 'woocommerce_after_template_part', 		array( $this, 'plugin_elementor_price_data' ), 10, 4 );
		}

		/******************************
		// Plugin Klarna Compabilities
		******************************/
		if ( function_exists( 'init_klarna_gateway' ) ) {
			add_action( 'german_market_after_frontend_init', array( $this, 'plugin_support_klarna' ) );
		}

		/******************************
		// Plugin WPGlobus Compabilities
		******************************/
		if ( defined( 'WPGLOBUS_VERSION' ) || defined( 'WOOCOMMERCE_WPGLOBUS_VERSION' ) ) {
			update_option( 'german_market_attribute_in_product_name', 'on' );
			add_filter( 'woocommerce_de_ui_options_products', array( $this, 'wpglobus_attribute_in_product_name' ) );
		}

		/******************************
		// Plugin Woo Floating Cart
		******************************/
		if ( defined( 'WOOFC_VERSION' ) ) {
			add_action( 'german_market_after_frontend_init', array( $this, 'plugin_woo_floating_cart' ) );
		}

		/******************************
		// Plugin YITH WooCommerce Added to Cart Popup Premium
		******************************/
		if ( defined( 'YITH_WACP_VERSION' ) || defined( 'YITH_WACP_PREMIUM' ) ) {
			add_filter( 'gm_add_virtual_product_notice_not_in_ajax', '__return_true' );
		}

		/******************************
		// Plugin WooCommerce Product Bundles
		******************************/
		if ( class_exists( 'WC_Bundles' ) ) {
			add_filter( 'gm_add_mwst_rate_to_product_item_return', array( $this, 'wc_bundles_gm_add_mwst_rate_to_product_item_return' ), 10, 3 );
		}

		/******************************
		// Plugin WooCommerce Product Bundles
		******************************/
		if ( function_exists( 'woocommerce_heidelpaycw_loaded' ) ) {
			add_filter( 'wgm_double_opt_in_activation_user_roldes', array( $this, 'wgm_double_opt_in_activation_user_roldes_heideplaycw' ) );
		}

		/******************************
		// Plugin Wirecard Checkout Seamless
		******************************/
		if ( function_exists( 'init_woocommerce_wcs_gateway' ) ) {
			add_filter( 'gm_2ndcheckout_gateway_label', array( $this, 'wcs_gateway_2ndcheckout_label' ) );
		}

		/******************************
		// Plugin WooCommerce Bookings
		******************************/
		if ( class_exists( 'WC_Bookings' ) ) {
			add_filter( 'wp_wc_infoice_pdf_item_meta_end_markup', array( $this, 'wc_bookings_wp_wc_infoice_pdf_item_meta_end_markup' ), 10, 4 );
		}

		do_action( 'wgm_compatibilities_after_construct', $this );
	}

	/**
	* WPBakeryVisualComposer: Price Infos after Price
	*
	* @since v3.8.1
	* @wp-hook woocommerce_get_price_html
	* @param String $price
	* @param WC_Product $product
	* @return String
	*/
	function wp_bakery_woocommerce_get_price_html( $price, $product ) {

		$debug_backtrace = debug_backtrace( DEBUG_BACKTRACE_PROVIDE_OBJECT, 10 );

		foreach ( $debug_backtrace as $elem ) {
			
			if ( $elem[ 'function' ] == 'vc_do_shortcode' ) {

				ob_start();
				add_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_loop' ), 10, 3 );
				echo WGM_Template::get_wgm_product_summary( $product );
				remove_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_loop' ), 10, 3 );
				$price .= ob_get_clean();

				break;
			}
		}

		return $price;
	}

	/**
	* Theme Ciloe: In Loop: Change order of data
	*
	* @since v3.8.1
	* @tested with theme Version 1.5.0
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_ciloe() {
		remove_action( 'woocommerce_after_shop_loop_item_title', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ), 5 );
		
	}

	/**
	* Theme DFD Native: Double Price
	*
	* @since v3.8.1
	* @tested with theme Version 1.4.0
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_dfd_native() {
		
		remove_action('woocommerce_after_shop_loop_item_title', 'dfd_woocommerce_template_loop_price', 5 );

		add_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_single' ), 10, 3 );
		remove_action( 'woocommerce_single_product_summary', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 7 );
		add_action( 'woocommerce_single_product_summary', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 21 );

	}

	/**
	* Theme Page Builder Framework: Double Price in Loop
	*
	* @since v3.8.1
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_page_builder_framework() {

		// shop
		add_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_loop' ), 10, 3 );
		remove_action( 'woocommerce_after_shop_loop_item_title', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ), 5 );
		add_action( 'wpbf_woo_loop_after_price',array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ) );

	}

	/**
	* Plugin WooCommerce Bookings: Show Data in Invoice PDF
	*
	* @since v3.8.1
	* @tested with plugin version 1.12.2
	* @wp-hook wp_wc_infoice_pdf_item_meta_end_markup
	* @return void
	*/
	function wc_bookings_wp_wc_infoice_pdf_item_meta_end_markup( $item_meta_end, $item_id, $item, $order ) {
		return str_replace( ' &rarr;', '', $item_meta_end );
	}

	/**
	* Theme Justshop: Double Price on Product Page
	*
	* @since v3.8.1
	* @tested with theme version 4.6
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_jutshop() {
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 20 );
	}

	/**
	* Theme Planetshine Polaris: Double Price on Product Page and Quickview
	*
	* @since v3.8.1
	* @tested with theme version 1.1.36
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_planetshine_polaris() {
		add_action( 'woocommerce_before_single_product_summary', array( $this, 'theme_support_planetshine_polaris_quickview' ) );
	}

	/**
	* Theme Planetshine Polaris: Double Price on Product Page and Quickview
	*
	* @since v3.8.1
	* @tested with theme version 1.1.36
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_planetshine_polaris_quickview() {
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 7 );
	}

	/**
	* Theme Hypermarket: Replace GM Info in Loop
	*
	* @since v3.8.1
	* @tested with theme version 1.5.5
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_hypermarket() {
		remove_action( 'woocommerce_after_shop_loop_item_title', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ), 5 );
		add_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_loop' ), 10, 3 );
		add_action('woocommerce_after_shop_loop_item', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ), 31 );
	}

	/**
	* Theme Electro
	*
	* @since v3.8.1
	* @tested with theme version 2.2.3
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_electro() {
		add_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_single' ), 10, 3 );
		remove_action( 'woocommerce_single_product_summary', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 7 );
		add_action( 'electro_single_product_action', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 21 );

	}

	/**
	* Theme Adorn: Doubled Price on product page and loop
	*
	* @since v3.7.2
	* @tested with theme version 1.1
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_adorn() {

		add_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_loop' ), 10, 3 );
		remove_action( 'woocommerce_after_shop_loop_item_title', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ), 5 );
		add_action( 'adorn_edge_woo_pl_info_below_image', array( $this, 'theme_support_adorn_loop_price' ), 27 ); 

		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 8 );
		
	}

	/**
	* Theme Adorn: Correct Loop Data
	*
	* @since v3.7.2
	* @tested with theme version 1.1
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_adorn_loop_price() {
		echo '<span class="price" style="display: block;">';
			WGM_Template::woocommerce_de_price_with_tax_hint_loop();
		echo '</span>';
	}

	/**
	* Theme Variegated: Doubled Price on product page and loop
	*
	* @since v3.7.2
	* @tested with theme version 1.0.0
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_variegated() {
		
		add_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_loop' ), 10, 3 );
		remove_action( 'woocommerce_after_shop_loop_item_title', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ), 5 );
		add_action( 'woocommerce_after_shop_loop_item', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ), 3 ); 

		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 11 );
		
	}

	/**
	* Theme Variegated: Doubled Price on product page and loop, reorder on single product
	*
	* @since v3.7.2
	* @tested with theme version 1.0.0
	* @wp-hook wp_head
	* @return void
	*/
	function theme_support_variegated_css() {
		?>
		<style>
			.woocommerce.single-product div.product .product_title, .woocommerce .single-product div.product .product_title {
				order: 0;
			}
		</style>
		<?php
	}

	/**
	* Divi Page Builder: JS Conflicts
	*
	* @since v3.7.2
	* @wp-hook wp_enqueue_scripts
	* @return void
	*/
	function divi_page_builder() {
		
		if ( has_action( 'wp_footer', 'et_fb_wp_footer' ) ) {
			wp_dequeue_script( 'woocommerce_de_frontend' );
		}
		
	}

	/**
	* Theme Ordo: Doubled Price on product page
	*
	* @since v3.7.2
	* @tested with theme version 1.1.4
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_ordo() {
		add_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_loop' ), 10, 3 );
		remove_action( 'woocommerce_after_shop_loop_item_title', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ), 5 );
		add_action( 'ftc_after_shop_loop_item', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ), 90 );
		remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 6);
	}

	/**
	* Theme Erado: Doubled Price on product page
	*
	* @since v3.7.2
	* @tested with theme version 1.0
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_erado() {
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 7 );
	}

	/**
	* Theme Supro: Doubled Price on product page
	*
	* @since v3.7.2
	* @tested with theme version 20.9.5.2
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_supro() {
		remove_action( 'woocommerce_single_product_summary', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 7 );
		add_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_single' ), 10, 3 );
		add_action( 'woocommerce_single_product_summary', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 11 );
	}

	/**
	* Plugins YITH WooCommerce Best Sellers & YITH WooCommerce Wishlist: Doubled [Digital]
	*
	* @since v3.7.2
	* @tested with plugin YITH WooCommerce Best Sellers Premium version 1.1.4
	* @tested with plugin YITH WooCommerce Wishlist Premium version 2.2.4
	* @wp-hook woocommerce_product_title
	* @param String $title
	* @param WC_Product $product
	* @return String
	*/
	function plugins_yith_wl_bs( $title, $product ) {
		return str_replace( '[Digital] [Digital]', '[Digital]', $title );
	}

	/**
	* Theme Technigs: Doubled Price in loop and product page
	*
	* @since v3.7.2
	* @tested with theme version 1.0.0
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_technics() {

		// Loop
		add_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_loop' ), 10, 3 );
		remove_action( 'woocommerce_after_shop_loop_item_title', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ), 5 );
		add_action( 'woocommerce_after_shop_loop_item', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ), 3 );

		// Single
		remove_action( 'woocommerce_single_product_summary', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 7 );
		add_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_single' ), 10, 3 );
		add_action( 'woocommerce_single_product_summary', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 12 );

	}

	/**
	* Theme Bazien: Doubled Price in loop and product page
	*
	* @since v3.7.2
	* @tested with theme version 2.5
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_bazien() {
		
		// Loop
		add_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_loop' ), 10, 3 );

		// Single
		remove_action( 'woocommerce_single_product_summary', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 7 );
		add_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_single' ), 10, 3 );
		add_action( 'woocommerce_single_product_summary_single_price', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 11 );
		
	}

	/**
	* Theme Ecode: Doubled Price in loop and product page
	*
	* @since v3.7.1
	* @tested with theme version 1.1.4
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_alishop() {
		remove_action( 'woocommerce_after_shop_loop_item_title', 'alishop_template_loop_price', 10 );
		remove_action( 'woocommerce_single_product_summary', 'alishop_woocommerce_single_price', 10 );
	}

	/**
	* Theme Ecode: Doubled Price in loop
	*
	* @since v3.7.1
	* @tested with theme version 1.0
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_ecode() {

		// shop
		add_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_loop' ), 10, 3 );
		remove_action( 'woocommerce_after_shop_loop_item_title', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ), 5 );
		add_action( 'woocommerce_after_shop_loop_item', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ), 10 );
	}

	/**
	* Theme Astra: Doubled Price in loop
	*
	* @since v3.7.1
	* @tested with theme version 1.4.10
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_astra() {
		
		// shop
		add_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_loop' ), 10, 3 );
		remove_action( 'woocommerce_after_shop_loop_item_title', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ), 5 );
		add_action( 'astra_woo_shop_price_after',array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ) );

		if ( class_exists( 'ASTRA_Ext_WooCommerce_Markup' ) ) {

			// single
			add_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_single' ), 10, 3 );
			remove_action( 'woocommerce_single_product_summary', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 7 );
			add_action( 'astra_woo_single_price_after', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 7 );

		}
	}

	/**
	* Theme OceanWP: Doubled Price in loop and single
	*
	* @since v3.7.1
	* @tested with theme version 1.5.27
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_oceanwp() {
		
		// shop
		add_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_loop' ), 10, 3 );
		remove_action( 'woocommerce_after_shop_loop_item_title', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ), 5 );
		add_action( 'ocean_after_archive_product_inner',array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ) );

		// single
		add_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_single' ), 10, 3 );
		remove_action( 'woocommerce_single_product_summary', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 7 );
		add_action( 'ocean_after_single_product_price', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ) );

	}
	/**
	* Theme Flatsome: Add German Market Data after single price
	*
	* @since 	v3.7.2
	* @tested with theme version 3.6.1
	* @wp-hook 	woocommerce_after_template_part
	* @param 	String $template_name
	* @param 	String $template_path
	* @param 	String $located
	* @param 	Array $args	
	* @return 	void
	*/
	function theme_flatsome_price_data( $template_name, $template_path, $located, $args ) {

		if ( $template_name == 'single-product/price.php' || $template_name == '/single-product/price.php' ) {
			
			$debug_backtrace = debug_backtrace( DEBUG_BACKTRACE_PROVIDE_OBJECT, 1 );
			
			if ( isset( $debug_backtrace[ 0 ][ 'function' ] ) && ( $debug_backtrace[ 0 ][ 'function' ] === 'theme_flatsome_price_data' ) ) {
				
				if ( apply_filters( 'german_market_compatibility_elementor_price_data', true ) ) {

					add_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_loop' ), 10, 3 );
					add_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_single' ), 10, 3 );
					WGM_Template::woocommerce_de_price_with_tax_hint_single();
					remove_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_single' ), 10, 3 );
					remove_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_loop' ), 10, 3 );
				
				}
				
			}

		}

	}

	/**
	* Plugin Elementor: Add German Market Data after single price
	*
	* @since 	v3.7.2
	* @tested with plugin version 2.2.5
	* @wp-hook 	woocommerce_after_template_part
	* @param 	String $template_name
	* @param 	String $template_path
	* @param 	String $located
	* @param 	Array $args	
	* @return 	void
	*/
	function plugin_elementor_price_data( $template_name, $template_path, $located, $args ) {

		if ( has_action( 'after_setup_theme', array( $this, 'theme_support_oceanwp' ) ) ) {
			return;
		}

		if ( $template_name == 'single-product/price.php' || $template_name == '/single-product/price.php' ) {
			
			$debug_backtrace = debug_backtrace( DEBUG_BACKTRACE_PROVIDE_OBJECT, 1 );
			
			if ( isset( $debug_backtrace[ 0 ][ 'function' ] ) && ( $debug_backtrace[ 0 ][ 'function' ] === 'plugin_elementor_price_data' ) ) {
				
				if ( apply_filters( 'german_market_compatibility_elementor_price_data', true ) ) {

					add_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_single' ), 10, 3 );
					WGM_Template::woocommerce_de_price_with_tax_hint_single();
					remove_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_single' ), 10, 3 );
				
				}
				
			}

		}

	}

	/**
	* Plugin Elementor: Remove Filters from Plugin
	* before sending Email with German Market Content
	*
	* @since v3.7.1
	* @tested with plugin version 2.1.6
	* @wp-hook wgm_email_before_get_email_de_footer
	* @return void
	*/
	function plugin_elementor_remove_filters() {
		
		$elementor = Elementor\Plugin::instance();
		$elementor->frontend->remove_content_filter();

	}

	/**
	* Plugin Elementor: Add filters again from plugin 
	* after sending Email with German Market Content
	*
	* @since v3.7.1
	* @tested with plugin version 2.1.6
	* @wp-hook wgm_email_after_get_email_de_footer
	* @return void
	*/
	function plugin_elementor_add_filters_again() {

		$elementor = Elementor\Plugin::instance();
		$elementor->frontend->add_content_filter();

	}

	/**
	* Theme Amely: Doubled Price in loop and singe product pages
	*
	* @since v3.7
	* @tested with theme version 1.6.1
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_amely() {
		remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 5 );
		add_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_single' ), 10, 3 );
	}

	/**
	* Plugin Wirecard Checkout Seamless: Show chosen payment type label on 2nd Checkout Page
	*
	* @since v3.7
	* @tested with plugin version 1.0.18
	* @wp-hook gm_2ndcheckout_gateway_label
	* @paran String $label
	* @return String
	*/
	function wcs_gateway_2ndcheckout_label( $label ) {

		$payment_type = WGM_Session::get( 'wcs_payment_method', 'first_checkout_post_array' );
		$paymentClass = 'WC_Gateway_Wirecard_Checkout_Seamless_'. str_replace( '-', '_', ucfirst( strtolower( $payment_type ) ) );
		$paymentClass = new $paymentClass( array() );
		return $paymentClass->get_label();

	}

	/**
	* Theme iustore: Doubled Price in loop and singe product pages
	*
	* @since v3.6.4
	* @tested with theme version 1.8
	* @wp-hook init
	* @return void
	*/
	function theme_support_iustore() {
		remove_action( 'woocommerce_single_product_summary','woocommerce_template_single_price',15 );
		add_filter( 's7upf_product_price', array( $this, 's7upf_product_price' ) );
	}

	/**
	* Theme iustore: Remove Theme Price
	*
	* @since v3.6.4
	* @tested with theme version 1.8
	* @wp-hook s7upf_product_price
	* @return void
	*/
	function s7upf_product_price( $html ) {
		return '';
	}
	
	/**
	* Theme Elessi: handsome-shop
	*
	* @since v3.6.3
	* @tested with theme version 1.0.9
	* @wp-hook init
	* @return void
	*/
	function theme_support_handmade_shop() {
		add_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_loop' ), 10, 3 );
		add_action( 'wgm_product_summary_parts', array( $this, 'theme_support_wgm_remove_price_in_summary_parts_in_shop' ), 10, 3 );
	}

	/**
	* Theme Kalium: Price in Loop & Product Pages
	*
	* @since v3.6.3
	* @tested with theme version 2.5.0
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_kalium() {

		// Loop
		add_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_loop' ), 10, 3 );
		remove_action( 'woocommerce_after_shop_loop_item_title', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ), 5 );
		add_action( 'woocommerce_after_template_part', array( $this, 'theme_support_kalium_loop_price' ), 10, 4 );

		// Shop
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 29 );
	}

	/**
	* Theme Kalium: Price in Loop
	*
	* @since v3.6.3
	* @tested with theme version 2.5.0
	* @wp-hook woocommerce_after_template_part
	* @param String $template_name
	* @param String $template_path
	* @param String $located
	* @param Array $args
	* @return void
	*/
	function theme_support_kalium_loop_price( $template_name, $template_path, $located, $args ){

		if ( $template_name == 'loop/price.php' ) {
			WGM_Template::woocommerce_de_price_with_tax_hint_loop();
		}

	} 

	/**
	* Plugin HeidelpayCw
	*
	* With this Plugin registered customer have the user role "Subscriber" instead of "Customer"
	*
	* @since v3.6.3
	* @tested with plugin version 3.0.182
	* @wp-hook wgm_double_opt_in_activation_user_roldes
	* @return void
	*/
	function wgm_double_opt_in_activation_user_roldes_heideplaycw( $user_roles ) {
		$user_roles[] = 'Subscriber';
		return $user_roles;
	}

	/**
	* Theme Zass: Price in Loop & Product Pages
	*
	* @since v3.6.3
	* @tested with theme version 2.7.0
	* @wp-hook german_market_after_frontend_init
	* @return void
	*/
	function theme_support_zass() {
		add_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_loop' ), 10, 3 );
		remove_action( 'woocommerce_after_shop_loop_item_title', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ), 5 );
		add_action( 'woocommerce_after_template_part', array( $this, 'theme_support_zass_after_price_loop' ), 10, 1 );
	}

	/**
	* Theme Zass: Price in Loop & Product Pages
	*
	* @since v3.6.3
	* @tested with theme version 2.7.0
	* @wp-hook woocommerce_after_template_part
	* @param String $template_name
	* @return void
	*/
	function theme_support_zass_after_price_loop( $template_name ) {

		if ( $template_name == 'loop/price.php' ) {
			echo '<div style="german-market-product-info-loop-price">';
				WGM_Template::woocommerce_de_price_with_tax_hint_loop();
			echo '</div>';
		}
	}

	/**
	* Theme Ronneby: Price in Loop & Product Pages
	*
	* @since v3.6.3
	* @tested with theme version 2.4.7
	* @wp-hook init
	* @return void
	*/
	function theme_support_ronneby() {
		remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 11 );
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 15 );
	}

	/**
	* Theme Elessi: Price in Loop & Product Pages
	*
	* @since v3.6.2
	* @tested with theme version 1.0.5
	* @wp-hook ini
	* @return void
	*/
	function theme_support_elessi() {
		remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 20);
		remove_action('woocommerce_after_shop_loop_item_title', 'elessi_loop_product_price', 10);
	}

	/**
	* Plugin WooCommerce Product Bundles: Don't show taxes of bundled items
	*
	* @since v3.5.9
	* @wp-hook gm_add_mwst_rate_to_product_item_return
	* @param Boolean $booleand
	* @param WC_Prodcut $product
	* @param Array $item
	* @return Bollean
	*/
	function wc_bundles_gm_add_mwst_rate_to_product_item_return( $boolean, $product, $item ) {

		if ( isset( $item[ 'bundled_by' ] ) ) {
			$boolean = true;
		}

		return $boolean;

	}

	/**
	* Theme Peony: Price in Loop
	*
	* @since v3.5.9
	* @wp-hook wp
	* @return void
	*/
	function theme_support_peony() {
		add_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_loop' ), 10, 3 );
	}

	/**
	* Themes that uses an old version of 'cart/cart.php' of German Market 
	* AND / OR
	* uses out cart table with tasxes but uses 'woocommerce_cart_item_subtotal' hook to display subtotals
	*
	* @since v3.5.8
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_cart_template_remove_taxes_in_subototal() {
		remove_action( 'woocommerce_checkout_init', array( 'WGM_Template', 'add_mwst_rate_to_product_item_init' ) );

	}

	/**
	* Theme Worldmart: Price in Product Pages
	*
	* @since v3.5.7
	* @wp-hook wp
	* @return void
	*/
	function theme_support_worldmart() {
		remove_action( 'woocommerce_single_product_summary', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 7 );
		add_action( 'woocommerce_single_product_summary', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 21 );
		add_action( 'wgm_product_summary_parts', array( $this, 'theme_support_wgm_remove_price_in_summary_parts_in_shop' ), 10, 3 );
	}

	/**
	* Theme XStore: Price in Product Pages
	*
	* @since v3.5.7
	* @wp-hook wp
	* @return void
	*/
	function theme_support_xstore() {

		if ( is_product() ) {

			$product = wc_get_product();

			if ( $product->get_type() == 'variable' ) {

				remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 25 );

			} else {

				add_action( 'wgm_product_summary_parts', array( $this, 'theme_support_wgm_remove_price_in_summary_parts_in_shop' ), 10, 3 );
				remove_action( 'woocommerce_single_product_summary', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 7 );
				add_action( 'woocommerce_single_product_summary', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 26 );

			}

		}

	}

	/**
	* Theme Sober: Price in Shop
	*
	* @since v3.5.6
	* @wp-hook german_market_after_frontend_init
	* @return void
	*/
	function theme_support_sober(){
		
		add_action( 'wgm_product_summary_parts', array( $this, 'theme_support_wgm_remove_price_in_summary_parts_in_shop' ), 10, 3 );
		remove_action( 'woocommerce_single_product_summary', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 7 );
		add_action( 'woocommerce_single_product_summary', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 21 );
	} 

	/**
	* Theme Support: Remove GM Price in Shop
	*
	* @since v3.5.3
	* @wp-hook wgm_product_summary_parts
	* @param Array $output_parts
	* @param WC_Product $product
	* @param String $hook
	* @return Array
	*/
	function theme_support_wgm_remove_price_in_summary_parts_in_shop( $output_parts, $product, $hook ) {

		if ( $hook == 'single' ) {

			if ( isset( $output_parts[ 'price' ] ) ) {
				unset( $output_parts[ 'price' ] );
			}

		}

		return $output_parts;
	}

	/**
	* WPML: Translate delivery times
	*
	* @since v3.5.5
	* @wp-hook woocommerce_de_get_deliverytime_label_term
	* @param Object $label_term
	* @param WC_Product $product
	* @return void
	*/
	function wpml_translate_delivery_times( $label_term, $product ) {
		
		if ( apply_filters( 'gm_wpml_has_wrong_settings_for_label_terms', false ) ) {
			return $label_term;
		}
		
		global $sitepress;
		$default_wpml_language = $sitepress->get_default_language();

		if ( ! method_exists( $product, 'get_id' ) ) {
			return $label_term;
		}

		$default_language_product_id = icl_object_id( $product->get_id(), get_post_type( $product->get_id() ), false, $default_wpml_language );
		if ( $default_language_product_id > 0 ) {
			$term_id = WGM_Template::get_term_id_from_product_meta( '_lieferzeit', wc_get_product( $default_language_product_id ) );
			$new_term_id = icl_object_id( $term_id, 'product_delivery_times', true, $sitepress->get_current_language() );
			$label_term = get_term( $new_term_id, 'product_delivery_times' );
	
		}

		return $label_term;
	}

	/**
	* Theme The7: Price in Loop
	*
	* @since v3.5.5
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_the7() {
		remove_action( 'woocommerce_after_shop_loop_item_title', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ), 5 );
		add_action( 'woocommerce_after_shop_loop_item_title', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ), 10 );
		add_action( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_loop' ), 10, 3 );
	}

	/**
	* Theme Hestia Pro: Price in Loop
	*
	* @since v3.5.5
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_hestia_pro() {
		
		// double price in loop
		remove_action( 'woocommerce_after_shop_loop_item_title', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ), 5 );
		add_filter( 'wgm_product_summary_parts', array( $this, 'theme_support_hide_gm_price_in_loop' ), 10, 3 );
		add_action( 'woocommerce_after_shop_loop_item_title', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ), 20 );

		// double price in grouped products
		add_filter( 'gm_add_price_in_loop_for_grouped_products_again', '__return_false' );
	} 
	
	/**
	* Remove Price in Shop
	*
	* @since v3.5.4
	* wp-hook wgm_product_summary_parts
	* @param Array $parts
	* @param WC_Product $product
	* @param String $hook
	* @return String
	**/
	function theme_support_hide_gm_price_in_loop( $parts, $product, $hook ) {

		if ( $hook == 'loop' && isset( $parts[ 'price' ] ) ) {
			unset( $parts[ 'price' ] );
		}

		return $parts;

	}

	/**
	* Remove Price in Single Pages
	*
	* @since v3.7
	* wp-hook wgm_product_summary_parts
	* @param Array $parts
	* @param WC_Product $product
	* @param String $hook
	* @return String
	**/
	function theme_support_hide_gm_price_in_single( $parts, $product, $hook ) {

		if ( $hook == 'single' && isset( $parts[ 'price' ] ) ) {
			unset( $parts[ 'price' ] );
		}

		return $parts;
	}
	
	/**
	* Theme Kryia: Price in Loop
	*
	* @since v3.5.3
	* @wp-hook german_market_after_frontend_init
	* @return void
	*/
	function theme_kriya() {
		remove_action( 'woocommerce_after_shop_loop_item_title', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ), 5 );
		add_filter( 'wgm_product_summary_parts', array( $this, 'theme_kriya_wgm_product_summary_parts' ), 10, 3 );
		add_action( 'woocommerce_after_shop_loop_item', array( $this, 'theme_kriya_woocommerce_de_price_with_tax_hint_loop' ), 10 );
	}

	/**
	* Theme Kryia: Price in Loop, add DIVs
	*
	* @since v3.5.3
	* @wp-hook woocommerce_after_shop_loop_item
	* @return void
	*/
	function theme_kriya_woocommerce_de_price_with_tax_hint_loop() {

		global $product;
			
		if ( is_a( $product, 'WC_Product_Grouped' ) ) {
			return;
		}

		echo "<div class='product-details german-market-loop-infos-for-kriya-theme'>";
			echo WGM_Template::get_wgm_product_summary();
		echo "</div>";

	}

	/**
	* Theme Kryia: Price in Loop, don't show GM Price
	*
	* @since v3.5.3
	* @wp-hook wgm_product_summary_parts
	* @param Array $output_parts
	* @param WC_Product $product
	* @param String $hook
	* @return Array
	*/
	function theme_kriya_wgm_product_summary_parts( $output_parts, $product, $hook ) {

		if ( $hook == 'loop' ) {

			if ( isset( $output_parts[ 'price' ] ) ) {
				unset( $output_parts[ 'price' ] );
			}

		}

		return $output_parts;
	}

	/**
	* Theme Savoy: Payment Gateways in Checkout and TOC just once
	*
	* @version v3.6.2
	* @since v3.5.3
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_savoy() {

		if ( get_option( 'gm_deactivate_checkout_hooks', 'off' ) == 'off' ) {

			if ( apply_filters( 'gm_theme_support_savoy_deactivate_gm_checkout_hooks', true ) ) {
				update_option( 'gm_deactivate_checkout_hooks', 'on' );
			}

		}

		// remove_action( 'woocommerce_de_add_review_order', array( 'WGM_Template', 'terms_and_conditions' ) ); // changed in 3.6.2

	}

	/**
	* Theme VG Vegawine: Remove double price in shop
	*
	* @since v3.5.3
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_vegawine() {
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 15 );
	}

	/**
	* Theme Woo Floating Cart: Qunatity in mini cart does not show up
	*
	* @since v3.5.3
	* @wp-hook german_market_after_frontend_init
	* @return void
	*/
	function plugin_woo_floating_cart() {
		remove_action( 'woocommerce_widget_cart_item_quantity',	array( 'WGM_Template', 'mini_cart_price' ), 10, 3 );
	}

	/**
	* Theme Peddlar: Don't show any <a>-tags in product summary in loop
	*
	* @since v3.5.3
	* @wp-hook wgm_product_summary_html
	* @return void
	*/
	function theme_support_peddlar( $output_html, $output_parts, $product, $hook ) {
		return strip_tags( $output_html, '<p></span><div><del><ins><strong><small>' );
	}

	/**
	* Plugin WPGlobus: Always set the option "Product Attributes in product name" to on
	*
	* @since v3.5.3
	* @wp-hook woocommerce_de_ui_options_products
	* @param Array $options
	* @return Array
	*/
	function wpglobus_attribute_in_product_name( $options ) {

		$options[ 'attribute_in_product_name' ] = array(
			'name'     => __( 'Product Attributes in product name', 'woocommerce-german-market' ),
			'desc_tip' => __( 'As default, the variation attributes are shown in the product name since WooCommerce 3.0. If this option is deactivated, the attributes are shown separated under the product name.', 'woocommerce-german-market' ),
			'id'       => 'german_market_attribute_in_product_name',
			'type'     => 'wgm_ui_checkbox',
			'default'  => 'on',
			'desc'	   => __( 'Because of using the Plugin "WPGlobus" and / or "WooCommerce WPGlobus", this option cannot be turned off.', 'woocommerce-german-market' ),
			'custom_attributes' => array( 'disabled' => 'disabled' ),
		);

		return $options;

	}

	/**
	* Theme Woodstroid: Remove double price in shop
	*
	* @since v3.5.2
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_woodstroid() {
		remove_action( 'woocommerce_after_shop_loop_item', 'woostroid_woocommerce_template_loop_price_grid', 5 );
	}

	/**
	* Theme Envision: "Cart Estimate Notice" is shown twice, because the theme brings exactly the same notice
	*
	* @since v3.5.1
	* @wp-hook german_market_after_frontend_init
	* @return void
	*/
	function theme_support_envision() {
		remove_filter( 'woocommerce_proceed_to_checkout', array( 'WGM_Template', 'add_cart_estimate_notice' ), 0 );
	}

	/**
	* Plugin Polylang Support: Show delivery times in the correct way
	*
	* @since v3.5.1
	* @wp-hook woocommerce_de_get_deliverytime_string_label_string
	* @param String $string
	* @param WC_Product $product
	* @return String
	*/
	function polylang_woocommerce_de_get_deliverytime_string_label_string( $string, $product ) {
		
	    $term_id = WGM_Template::get_term_id_from_product_meta( '_lieferzeit', $product );

	    if ( (int) $term_id == - 1 || empty( $term_id ) ) {
	        $term_id = get_option( WGM_Helper::get_wgm_option( 'global_lieferzeit' ) );
	    }

	    $label_term = get_term( $term_id, 'product_delivery_times' );
	    
	    if ( is_wp_error( $label_term ) || ! isset( $label_term ) ) {
	        $label_string = __( 'not specified', 'woocommerce-german-market' );
	    } else {
	        $label_string = $label_term->name;
	    }

	    return $label_string;

	}

	/**
	* Plugin Klarna Support: Change behaviour how to send confirmation email
	*	
	* @since v3.5.1
	* @wp-hook german_market_after_frontend_init
	* @return void
	*/
	function plugin_support_klarna() {

		add_filter( 'gm_email_confirm_order_send_it', array( $this, 'klarna_email_confirm_order_send_it' ), 10, 2 );
		add_action( 'woocommerce_thankyou', array( $this, 'klarna_woocommerce_thankyou_send_email_confirm_order' ) );
	}

	/**
	* Plugin Klarna Support: Do not send order confirmation email at processed order
	*
	* @since v3.5.1
	* @wp-hook gm_email_confirm_order_send_it
	* @return void
	*/
	function klarna_email_confirm_order_send_it( $boolean, $order ) {

		$payment_method = $order->get_payment_method();
		if ( str_replace( 'klarna', '', $payment_method ) != $payment_method ) {
			$boolean = false;
		}

		return $boolean;
	}

	/**
	* Plugin Klarna Support: Send order confirmation email on thankyou page
	*
	* @since v3.5.1
	* @wp-hook woocommerce_thankyou
	* @return void
	*/
	function klarna_woocommerce_thankyou_send_email_confirm_order( $order_id ) {

		$order = wc_get_order( $order_id );
		$payment_method = $order->get_payment_method();
		
		if ( str_replace( 'klarna', '', $payment_method ) != $payment_method ) {

			if ( empty( $order->get_meta( '_gm_email_confirm_order_send' ) ) ) {
				
				WGM_Email::send_order_confirmation_mail( $order_id );
				$order->update_meta_data( '_gm_email_confirm_order_send', 'yes' );
				$order->save_meta_data();

			}

		}

	}

	/**
	* Theme Support woodance: Display prices correctly (not twice) on single products and variable product pages
	*
	* @wp-hook wp
	* @return void
	*/
	function theme_support_woodance() {
		
		if ( is_product() ) {

			$product = wc_get_product();

			if ( $product->get_type() == 'simple' ) {
				
				remove_action( 'woocommerce_single_product_summary', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 7 );
				add_action( 'woocommerce_before_add_to_cart_button', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 21 );
				remove_filter( 'wgm_product_summary_parts', array( 'WGM_Template', 'add_product_summary_price_part' ), 0, 3 );
			
			} else if ( $product->get_type() == 'variable' ) {

				remove_action( 'woocommerce_single_product_summary', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 7 );
				add_action( 'woocommerce_before_variations_form', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 21 );
				add_action( 'woocommerce_before_add_to_cart_form', array( $this, 'theme_support_woodance_no_price' ) );
				add_action( 'woocommerce_before_variations_form', array( $this, 'theme_support_woodance_no_price_add_again' ) );

			}

		}

	}

	/**
	* Theme Support fluent: Display prices correctly (not twice) on single products and variable product pages
	*
	* @wp-hook wp
	* @return void
	*/
	function theme_support_fluent() {
		
		if ( is_product() ) {

			$product = wc_get_product();

			if ( $product->get_type() == 'simple' ) {
				
				remove_action( 'woocommerce_single_product_summary', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 7 );
				add_action( 'woocommerce_single_product_summary', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 8 );
				remove_filter( 'wgm_product_summary_parts', array( 'WGM_Template', 'add_product_summary_price_part' ), 0, 3 );
			
			} else if ( $product->get_type() == 'variable' ) {

				remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 7 );

			}

		}

	}

	/**
	* Theme Support woodance: Display prices correctly (not twice) on single products and variable product pages
	* Remove Price html for variable products
	*
	* @wp-hook woocommerce_before_add_to_cart_form
	* @return void
	*/
	function theme_support_woodance_no_price() {
		add_filter( 'woocommerce_get_price_html', array( $this, 'theme_support_woodance_variable_price' ) );
	}

	/**
	* Theme Support woodance: Display prices correctly (not twice) on single products and variable product pages
	* Add Price html filter for variable products again
	*
	* @wp-hook woocommerce_before_variations_form
	* @return void
	*/
	function theme_support_woodance_no_price_add_again() {
		remove_filter( 'woocommerce_get_price_html', array( $this, 'theme_support_woodance_variable_price' ) );
	}

	/**
	* Theme Support woodance: Removes price html for variable product
	*
	* @wp-hook woocommerce_get_price_html
	* @return void
	*/
	function theme_support_woodance_variable_price( $price, $product ) {
		return '';
	}

	/**
	* Translateable Due Date Options
	*
	* @access public
	* @wp-hook init
	* @return void
	*/
	function due_date() {

		if ( ! is_admin() ) {
			return;
		}

		if ( ! ( isset( $_REQUEST[ 'page' ] ) && ( $_REQUEST[ 'page' ] == 'wc-settings' || $_REQUEST[ 'page' ] == 'german-market' ) ) ) {
			return;
		}

		$gateways = WC()->payment_gateways()->payment_gateways();
		$strings  = array();

		foreach ( $gateways as $payment_method_id => $gateway ) {

			if ( isset( $gateway->settings[ 'wgm_due_date_text' ] ) ) {
				$due_date_text 	= $gateway->settings[ 'wgm_due_date_text' ];
			} else {
				$due_date_text 	= apply_filters( 'woocommerce_de_due_date_text_' . $payment_method_id, __( 'Due Date: {{due-date}}', 'woocommerce-german-market' ) );
			}

			if ( function_exists( 'icl_register_string' ) && function_exists( 'icl_t' ) && function_exists( 'icl_st_is_registered_string' ) ) {

				if ( ! ( icl_st_is_registered_string( 'German Market: Due Date Option', $due_date_text ) ) ) {

					icl_register_string( 'German Market: Due Date Option', $due_date_text, $due_date_text );

				}

			} else if( function_exists( 'pll_register_string' ) && function_exists( 'pll__' ) ) {

					pll_register_string( $due_date_text, $due_date_text, 'German Market: Due Date Option', true );

			}

		}

	}

	/**
	* Theme Superba Support: Double price in loop and single product pages
	*
	* @access public
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_superba() {
		
		// avoid double price in loop
		remove_action( 'woocommerce_after_shop_loop_item', 			'thb_loop_product_end', 999 );
		remove_action( 'woocommerce_after_shop_loop_item_title', 	array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ), 5 );
		add_action( 'woocommerce_after_shop_loop_item', 			array( $this, 'superba_woocommerce_after_shop_loop_item' ), 999 );
		
		// avoid douple price in single product
		remove_action( 'woocommerce_single_product_summary',		array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 7 );
		remove_action('woocommerce_single_product_summary', 'thb_single_product_summary');
		add_action( 'woocommerce_single_product_summary', array( $this, 'superba_woocommerce_single_product_summary' ), 999 );
	}

	/**
	* Theme Superba Support: Double price in loop
	*
	* @access public
	* @wp-hook woocommerce_after_shop_loop_item
	* @return void
	*/
	function superba_woocommerce_after_shop_loop_item() {

		global $post, $product;
		$size = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
		echo $product->get_categories( ', ', '<span class="posted_in">', '</span>' );

		woocommerce_template_loop_rating();

		echo '<div class="thb-add-to-cart-wrapper">';
			WGM_Template::woocommerce_de_price_with_tax_hint_loop();
		echo "</div>";

		woocommerce_template_loop_add_to_cart();
		echo "</div>";

	}

	/**
	* Theme Superba Support: Double price on single product pages
	*
	* @access public
	* @wp-hook woocommerce_single_product_summary
	* @return void
	*/
	function superba_woocommerce_single_product_summary() {

		?>
		<div class="thb-product-header">
			<?php
				thb_pagination();
				woocommerce_breadcrumb();
				woocommerce_template_single_title();
				woocommerce_template_loop_rating();
				WGM_Template::woocommerce_de_price_with_tax_hint_single();
			?>
		</div>
		<div class="thb-product-description">
			<?php
				woocommerce_template_single_excerpt();
				woocommerce_template_single_add_to_cart();
			?>
		</div>
		<?php

	}

	/**
	* Woocommerce Subscriiption active filter wcs_new_order_created:filter_wcs_new_order_created each new Order, this new Order its same Subscription-Order, (Copy-Parent) 
	* we have to edit_wp_wc_running_invoice_number, _wp_wc_running_invoice_number_date in the new Order
	*
	* @since GM 3.4.3
	* @wp-hook wcs_new_order_created
	* @param WC_Order $order
	* @return WC_Order
	*/
    public function filter_wcs_new_order_created( $order ){

        if ( ( $order->get_id() != null ) && ( $order->get_id() == $this->_new_order_create ) ) {
            
            $date_process= get_post_meta( $order->get_id(), '_wp_wc_running_invoice_number_date', true );
            
            if ( (int)$this->_wp_wc_running_invoice_number_date > (int)$date_process ) {                
                $order->update_meta_data( '_wp_wc_running_invoice_number', $this->_wp_wc_running_invoice_number );
                $order->update_meta_data( '_wp_wc_running_invoice_number_date', $this->_wp_wc_running_invoice_number_date );                
            }            
        } 

        return $order;
    }

	/**
	* Wenn new Order is created, New_order = Parent_order_subscrition, then we save data and number, (this process with German-Marker its Okay, but this will replace in the new Filter).
	*
	* @since GM 3.4.3
	* @wp-hook woocommerce_new_order
	* @param WC_Order $order
	* @return WC_Order
	*/
    public function action_woocommerce_new_order($order){

        $this->_new_order_create = $order;
        $this->_wp_wc_running_invoice_number = get_post_meta( $order, '_wp_wc_running_invoice_number', true );
        $this->_wp_wc_running_invoice_number_date = get_post_meta( $order, '_wp_wc_running_invoice_number_date', true );
       
       return $order;
    }

	/**
	* Wenn new Order is created, New_order Parent_order_subscrition,
	* @since GM 3.4.3
	* @wp-hook woocommerce_countries_inc_tax_or_vat, woocommerce_countries_ex_tax_or_vat
	* @param String $return
	* @return String
	*/
	public function dummy_remove_woo_vat_notice( $return = "" ){

        if ( $return == "" ){
            $return = " ";
        }

        return $return;
    }

    /**
	* WPML Support: Switch language of invoice in backend downloads
	*
	* @since 3.8.1
	* @access public
	* @wp-hook wcreapdf_pdf_before_create
	* @param Array $args
	* @return void
	*/
    function wpml_invoice_pdf_admin_download_switch_lang( $args ) {
    	
    	if ( self::is_frontend_ajax() ) {
    		return;
    	}

    	if ( ! current_user_can( 'manage_woocommerce' ) ) {
    		return;
    	}

		global $sitepress;
		$order 		= $args[ 'order' ];
		$is_test 	= is_string( $args[ 'order' ] ) && $args[ 'order' ] == 'test';

		if ( ! $is_test ) {

			$order_language = $order->get_meta( 'wpml_language' );
			if ( ! empty( $order_language ) ) {
				$sitepress->switch_lang( $order_language );
			}

		}

    }

    /**
	* WPML Support: Reswitch language of invoice pdf in backend downloads
	*
	* @since 3.8.1
	* @access public
	* @wp-hook wp_wc_invoice_pdf_end_template
	* @return void
	*/
    function wpml_invoice_pdf_admin_download_reswitch_lang() {
    	
    	if ( self::is_frontend_ajax() ) {
    		return;
    	}

    	if ( ! current_user_can( 'manage_woocommerce' ) ) {
    		return;
    	}

    	global $sitepress;
    	$sitepress->switch_lang( $sitepress->get_default_language() );
    }

    /**
	* WPML Support: Switch language of retoure and delivery pdf in backend downloads
	*
	* @since 3.8.1
	* @access public
	* @wp-hook wcreapdf_pdf_before_create
	* @param Array $settings
	* @param WC_Order $order
	* @return void
	*/
    function wpml_retoure_pdf_admin_download_switch_lang( $delivery_or_retoure, $order ) {

    	if ( self::is_frontend_ajax() ) {
    		return;
    	}

    	if ( ! current_user_can( 'manage_woocommerce' ) ) {
    		return;
    	}

    	if ( ! method_exists( $order , 'get_meta' ) ) {
    		return;
    	}

		global $sitepress;

		$order_language = $order->get_meta( 'wpml_language' );
		if ( ! empty( $order_language ) ) {
			$sitepress->switch_lang( $order_language );
		}

    }

    /**
	* WPML Support: Reswitch language of retoure and delivery pdf in backend downloads
	*
	* @since 3.8.1
	* @access public
	* @wp-hook wcreapdf_pdf_after_create
	* @param Array $settings
	* @param WC_Order $order
	* @return Array
	*/
    function wpml_retoure_pdf_admin_download_reswitch_lang( $delivery_or_retoure, $order ) {

    	if ( self::is_frontend_ajax() ) {
    		return;
    	}

    	if ( ! current_user_can( 'manage_woocommerce' ) ) {
    		return;
    	}

    	global $sitepress;
    	$sitepress->switch_lang( $sitepress->get_default_language() );

    }

     /**
	* Returns true if ajax is executed from frontend
	*
	* @since 3.8.1
	* @access public
	* @return Boolean
	*/
    public static function is_frontend_ajax() {
    	
    	$script_filename = isset( $_SERVER[ 'SCRIPT_FILENAME' ] ) ? $_SERVER[ 'SCRIPT_FILENAME' ] : '';
 
		//Try to figure out if frontend AJAX request... If we are DOING_AJAX; let's look closer
		if ( ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {

	    	$ref = '';

			if ( ! empty( $_REQUEST[ '_wp_http_referer' ] ) ) {
				$ref = wp_unslash( $_REQUEST[ '_wp_http_referer' ] );
			} elseif ( ! empty( $_SERVER[ 'HTTP_REFERER' ] ) ) {
				$ref = wp_unslash( $_SERVER[ 'HTTP_REFERER' ] );
			}
	 
			// If referer does not contain admin URL and we are using the admin-ajax.php endpoint, this is likely a frontend AJAX request
			if ( ( ( strpos( $ref, admin_url() ) === false ) && ( basename( $script_filename ) === 'admin-ajax.php' ) ) ) {
			  return true;
			}
		}

		return false;

	}

    /**
	* WPML Support: Option how to handle backend downloads in backend
	*
	* @since 3.8.1
	* @access public
	* @wp-hook woocommerce_de_ui_options_global
	* @param Array $settings
	* @return Array
	*/
    function woocommerece_multilingual_invoice_pdf_admin_download( $settings ) {

    	$settings[] = array( 'title' => __( 'WPML Support', 'woocommerce-german-market' ), 'type' => 'title','desc' => '', 'id' => 'wp_wc_invoice_pdf_general_pdf_wpml' );

    	$settings[] = array(
						'name' 		=> __( 'Backend Download Language of Invoice, Delivery and Retoure PDFs', 'woocommerce-german-market' ),
						'id'   		=> 'wp_wc_invoice_pdf_wpml_admin_download_language',
						'type' 		=> 'select',
						'default'  	=> 'switcher',
						'options' 	=> array(
										'switcher'		=> __( 'Language of Admin Language Switcher', 'woocommerce-german-market' ),
										'landscape'		=> __( 'Order Language', 'woocommerce-german-market' )
									)
					);

    	$settings[] = array( 'type' => 'sectionend', 'id' => 'wp_wc_invoice_pdf_general_pdf_wpml' );

    	return $settings;
    }

	/**
	* Wocommerece Multilingual removes the languages switcher on "post_type=shop_order" but 
	* we need it to make invoice pdfs translatable
	*
	* @access public
	* @wp-hook current_screen
	* @return void
	*/
	function woocommerece_multilingual_add_language_switcher() {

		$screen = get_current_screen();
		
		if ( $screen->id == 'edit-shop_order' && $screen->base == 'edit' ) {
			
			global $sitepress;
			add_action( 'wp_before_admin_bar_render', array( $sitepress, 'admin_language_switcher' ), 20 );
			
		}

	}

	/**
	* Theme aurom Support: Double price in loop and single product pages
	*
	* Tested with Theme Version 3.0.1
	* @access public
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_aurum() {
		
		// avoid double price in loop
		remove_action( 'woocommerce_after_shop_loop_item_title', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ), 5 );
		remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_price', 20 );
		add_action( 'woocommerce_after_shop_loop_item',		array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 20 );

		// avoid douple price in single product
		remove_filter( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 25 );

		// avoid double taxes in cart
		add_filter( 'gm_cart_template_in_theme_show_taxes', '__return_false' );
	}

	/**
	* Theme Avada Support
	*
	* @access public
	* Tested with Theme Version 5.7.2
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_avada() {
		add_filter( 'woocommerce_cart_item_name', array( $this, 'theme_support_avada_remove_double_digital' ), 99, 3 );

		// removed in 3.8.1
		//add_filter( 'woocommerce_pay_order_button_html', array( $this, 'theme_support_avada_pay_for_order_page_checkboxes' ) );

	}

	/**
	* Theme Avada Support: Checkboxes on pay for order page
	*
	* @access public
	* @wp-hook woocommerce_pay_order_button_html
	* @param String $markup
	* @return String
	*/
	function theme_support_avada_pay_for_order_page_checkboxes( $markup ) {

		if ( is_wc_endpoint_url( 'order-pay' ) ) {

			$markup = WGM_Template::add_review_order() . $markup;

		}

		return $markup;

	}

	/**
	* Theme Avada Support: Double "[Digital]" during checkout, very simple solution
	*
	* @access public
	* @wp-hook woocommerce_cart_item_name
	* @return void
	*/
	function theme_support_avada_remove_double_digital( $title, $cart_item, $cart_item_key ) {
		return str_replace( '[Digital] [Digital]', '[Digital]', $title );
	}

	/**
	* WooCommerce Subscriptions Support: Don't copy invoice number, invoice date or due date form first order to subscription object
	*
	* @access public
	* @wp-hook wcs_renewal_order_meta
	* @param Array $meta
	* @param WC_Order $to_order
	* @param WC_Order $from_order
	* @return Array
	*/
	function subscriptions_gm_dont_copy_meta( $meta, $to_order, $from_order ) {

		$new_meta = array();

		foreach ( $meta as $meta_item ) {
			
			$key = $meta_item[ 'meta_key' ];

			if ( $key == '_wp_wc_running_invoice_number' || $key == '_wp_wc_running_invoice_number_date' || $key == '_wgm_due_date' ) {
				continue;
			}

			$new_meta[] = $meta_item;

		}

		return $new_meta;
	}

	/**
	* WooCommerce Subscriptions Support: Email Attachments
	*
	* @access public
	* @wp-hook gm_invoice_pdf_email_settings
	* @wp-hook gm_invoice_pdf_email_settings_additonal_pdfs
	* @param Array $options
	* @return Array
	*/
	function subscriptions_gm_invoice_pdf_email_settings( $options ) {

		$prefix = current_filter() == 'gm_invoice_pdf_email_settings_additonal_pdfs' ? '_add_pdfs' : '';

		$options[] = array( 'title' => __( 'WooCommerce Subscriptions Support', 'woocommerce-german-market' ), 'type' => 'title','desc' => '', 'id' => 'wp_wc_invoice_pdf_emails_subcriptions' . $prefix );

		$options[] = array(
			'name'		=> __( 'New Renewal Order', 'woocommerce-german-market' ),
			'id'   		=> 'wp_wc_invoice_pdf_emails_new_renewal_order' . $prefix,
			'type' 		=> 'wgm_ui_checkbox',
			'default'  	=> 'off',
		);

		$options[] = array(
			'name'		=> __( 'Processing Renewal Order', 'woocommerce-german-market' ),
			'id'   		=> 'wp_wc_invoice_pdf_emails_customer_processing_renewal_order' . $prefix,
			'type' 		=> 'wgm_ui_checkbox',
			'default'  	=> 'off',
		);

		$options[] = array(
			'name'		=> __( 'Complete Renewal Order', 'woocommerce-german-market' ),
			'id'   		=> 'wp_wc_invoice_pdf_emails_customer_completed_renewal_order' . $prefix,
			'type' 		=> 'wgm_ui_checkbox',
			'default'  	=> 'off',
		);

		$options[] = array(
			'name'		=> __( 'Customer Complete Switch Order', 'woocommerce-german-market' ),
			'id'   		=> 'wp_wc_invoice_pdf_emails_customer_completed_switch_order' . $prefix,
			'type' 		=> 'wgm_ui_checkbox',
			'default'  	=> 'off',
		);

		$options[] = array( 'type' => 'sectionend', 'id' => 'wp_wc_invoice_pdf_emails_subcriptions' . $prefix );

		return $options;
	}

	/**
	*  WooCommerce Subscriptions Support: Email Attachments for Retoure PDF
	*
	* @access public
	* @since 3.5.6
	* @wp-hook wcreapdf_email_options_after_sectioned
	* @param Array $options
	* @return Array
	*/
	function subscriptions_gm_retoure_pdf_email_settings( $options ) {


		$options[] = array( 'title' => __( 'WooCommerce Subscriptions Support', 'woocommerce-german-market' ), 'type' => 'title','desc' => '', 'id' => 'wp_wc_retoure_pdf_emails_subcriptions' );

		$options[] = array(
			'name'		=> __( 'New Renewal Order', 'woocommerce-german-market' ),
			'id'   		=> WCREAPDF_Helper::get_wcreapdf_optionname( 'new_renewal_order' ),
			'type' 		=> 'wgm_ui_checkbox',
			'default'  	=> 'off',
		);

		$options[] = array(
			'name'		=> __( 'Processing Renewal Order', 'woocommerce-german-market' ),
			'id'   		=> WCREAPDF_Helper::get_wcreapdf_optionname( 'customer_processing_renewal_order' ),
			'type' 		=> 'wgm_ui_checkbox',
			'default'  	=> 'off',
		);

		$options[] = array(
			'name'		=> __( 'Complete Renewal Order', 'woocommerce-german-market' ),
			'id'   		=> WCREAPDF_Helper::get_wcreapdf_optionname( 'customer_completed_renewal_order' ),
			'type' 		=> 'wgm_ui_checkbox',
			'default'  	=> 'off',
		);

		$options[] = array(
			'name'		=> __( 'Customer Complete Switch Order', 'woocommerce-german-market' ),
			'id'   		=> WCREAPDF_Helper::get_wcreapdf_optionname( 'customer_completed_switch_order' ),
			'type' 		=> 'wgm_ui_checkbox',
			'default'  	=> 'off',
		);

		$options[] = array( 'type' => 'sectionend', 'id' => 'wp_wc_retoure_pdf_emails_subcriptions' );

		return $options;
	}

	/**
	* WooCommerce Subscriptions Support: Email Attachments
	*
	* @access public
	* @wp-hook wp_wc_inovice_pdf_allowed_stati
	* @wp-hook wp_wc_inovice_pdf_allowed_stati_additional_mals
	* @param Array allowed_stati
	* @return Array
	*/
	function subscriptions_gm_allowed_stati_additional_mals( $allowed_stati ) {

		$allowed_stati[] = 'new_renewal_order';
		$allowed_stati[] = 'customer_processing_renewal_order';
		$allowed_stati[] = 'customer_completed_renewal_order';
		$allowed_stati[] = 'customer_completed_switch_order';

		return $allowed_stati;
	}

	/**
	* WooCommerce Subscriptions Support: Email Attachments in Add-Ons
	*
	* @access public
	* @wp-hook gm_emails_in_add_ons
	* @param Array allowed_stati
	* @return Array
	*/
	function subscriptions_gm_emails_in_add_ons( $emails ) {

		$emails[ 'new_renewal_order' ] 					= __( 'New Renewal Order', 'woocommerce-german-market' );
		$emails[ 'customer_processing_renewal_order' ]	= __( 'Processing Renewal Order', 'woocommerce-german-market' );
		$emails[ 'customer_completed_renewal_order' ]	= __( 'Complete Renewal Order', 'woocommerce-german-market' );

		return $emails;
	}

	/**
	* WooCommerce Subscriptions Support: Recurring Totals
	*
	* @access public
	* @wp-hook german_market_after_frontend_init
	*/
	function subscriptions() {
		remove_filter( 'woocommerce_cart_totals_order_total_html',	array( 'WGM_Template', 'woocommerce_cart_totals_excl_tax_string' ) );
	}
	
	/**
	* WPML Support: Translate WooCommerce Tax Rates for WPML
	*
	* @access public
	* @wp-hook woocommerce_find_rates
	* @param Array $matched_tax_rates
	* @return Array
	*/
	function translate_woocommerce_find_rates( $matched_tax_rates ) {

        foreach( $matched_tax_rates as &$rate ) {
 				
                if ( $rate[ 'label' ] ) {
                    $rate[ 'label' ] = icl_t( 'German Market: WooCommerce Tax Rate', 'tax rate label: ' . $rate[ 'label' ], $rate[ 'label' ] );  
                }

                unset($rate);

        }

        reset($matched_tax_rates);
 
        return $matched_tax_rates;
 
	}

	/**
	* WPML Support: Translate WooCommerce Checkout Strings
	*
	* @access public
	* @wp-hook option_{option}
	* @param String $value
	* @param String $option
	* @return String
	*/
	function translate_woocommerce_checkout_options( $value, $option ) {
		
		global $sitepress;
		$lang = $sitepress->get_current_language();
		$default_lang = $sitepress->get_default_language();

		if ( $lang == $default_lang ) {
			return $value;
		}

		if ( str_replace( 'wp_wc_invoice_pdf_', '', $option ) != $option ) {
			$value = apply_filters( 'wpml_translate_single_string', $value, 'German Market: Invoice PDF', $option, $lang );

		} else if ( str_replace( 'wp_wc_running_invoice_', '', $option ) != $option ) {
			$value = apply_filters( 'wpml_translate_single_string', $value, 'German Market: Running Invoice Number', $option, $lang );

		} else if ( ( str_replace( 'woocomerce_wcreapdf_wgm_', '', $option ) != $option ) || ( str_replace( 'woocommerce_wcreapdf_wgm_', '', $option ) != $option  ) ) {
			$value = apply_filters( 'wpml_translate_single_string', $value, 'German Market: Return Delivery Note', $option, $lang );

		} else {

			$value = apply_filters( 'wpml_translate_single_string', $value, 'German Market: Checkout Option', $option, $lang );
		}

		return $value;

	}

	/**
	* Polylang Support: Translate WooCommerce Checkout Strings
	*
	* @access public
	* @wp-hook option_{option}
	* @param String $value
	* @param String $option
	* @return String
	*/
	function translate_woocommerce_checkout_options_polylang( $value, $option ) {

		$value = pll__( $value );
		return $value;

	}

	/**
	* WPMP Support: Translate Tax Labels for order items
	*
	* @access public
	* @wp-hook option_{wgm_translate_tax_label}
	* @param String $tax_label
	* @return String
	*/
	function translate_tax_label( $tax_label ) {

		// WPML
		if ( function_exists( 'icl_register_string' ) && function_exists( 'icl_t' ) && function_exists( 'icl_st_is_registered_string' ) ) {
			$tax_label = icl_t( 'German Market: WooCommerce Tax Rate', 'tax rate label: ' . $tax_label, $tax_label );
		}

		return $tax_label;
	}

	/**
	* Custom Emails: Email Attachments
	*
	* @access public
	* @since 3.5.6
	* @wp-hook gm_invoice_pdf_email_settings
	* @wp-hook gm_invoice_pdf_email_settings_additonal_pdfs
	* @param Array $options
	* @return Array
	*/
	function custom_email_status_gm_invoice_pdf_email_settings( $options ) {

		$custom_mails = $this->get_custom_emails();
		
		if ( ! empty( $custom_mails ) ) {

			$prefix = current_filter() == 'gm_invoice_pdf_email_settings_additonal_pdfs' ? '_add_pdfs' : '';

			$options[] = array( 'title' => __( 'Custom Emails', 'woocommerce-german-market' ), 'type' => 'title','desc' => '', 'id' => 'wp_wc_invoice_pdf_emails_custom' . $prefix );

			foreach ( $custom_mails as $custom_mail ) {

				$options[] = array(
					'name'			=> $custom_mail[ 'title' ],
					'desc_tip'		=> $custom_mail[ 'description' ],
					'id'   			=> 'wp_wc_invoice_pdf_emails_' . $custom_mail[ 'id' ] . $prefix,
					'type' 			=> 'wgm_ui_checkbox',
					'default'  		=> 'off',
				);
			}

			$options[] = array( 'type' => 'sectionend', 'id' => 'wp_wc_invoice_pdf_emails_custom' . $prefix );

		}

		return $options;
	}

	/**
	* Custom Emails: Email Attachments for Retoure PDF
	*
	* @access public
	* @since 3.5.6
	* @wp-hook wcreapdf_email_options_after_sectioned
	* @param Array $options
	* @return Array
	*/
	function custom_email_status_gm_retoure_pdf_email_settings( $options ){

		$custom_mails = $this->get_custom_emails();
		
		if ( ! empty( $custom_mails ) ) {

			$options[] = array( 'title' => __( 'Custom Emails', 'woocommerce-german-market' ), 'type' => 'title','desc' => '', 'id' => 'wcreapdf_email_custom' );

			foreach ( $custom_mails as $custom_mail ) {

				$options[] = array(
					'name'			=> $custom_mail[ 'title' ],
					'desc_tip'		=> $custom_mail[ 'description' ],
					'id'   			=>  WCREAPDF_Helper::get_wcreapdf_optionname( $custom_mail[ 'id' ] ),
					'type' 			=> 'wgm_ui_checkbox',
					'default'  		=> 'off',
				);
			}

			$options[] = array( 'type' => 'sectionend', 'id' => 'wcreapdf_email_custom' );

		}

		return $options;
	} 

	/**
	* Get Custom Emails
	*
	* @access private
	* @since 3.5.6
	* @return Array
	*/
	private function get_custom_emails() {

		$all_mails 		= WC()->mailer()->get_emails();
		$custom_mails 	= array();

		foreach ( $all_mails as $key => $a_mail ) {
			
			if ( substr( $key, 0, 3 ) == 'WC_' || substr( $key, 0, 4 ) == 'WCS_' ) {
				continue;
			}

			$custom_mails[ $key ] = array(
				'title'			=> $a_mail->title,
				'description'	=> $a_mail->description,
				'id'			=> $a_mail->id,
			);
		}

		return $custom_mails;

	}

	/**
	* Custom Emails - Attachments
	*
	* @access public
	* @since 3.5.6
	* @wp-hook wp_wc_inovice_pdf_allowed_stati
	* @wp-hook wp_wc_inovice_pdf_allowed_stati_additional_mals
	* @param Array allowed_stati
	* @return Array
	*/
	function custom_email_status_gm_allowed_stati_additional_mals( $allowed_stati ) {

		$custom_mails = $this->get_custom_emails();

		foreach ( $custom_mails as $a_mail ) {
			$allowed_stati[] = $a_mail[ 'id' ];
		}

		return $allowed_stati;
	}

	/**
	* Custom Emails: Email Attachments in Add-Ons
	*
	* @access public
	* @since 3.5.6
	* @wp-hook gm_emails_in_add_ons
	* @param Array allowed_stati
	* @return Array
	*/
	function custom_email_status_gm_emails_in_add_ons( $emails ) {

		$custom_mails = $this->get_custom_emails();

		foreach ( $custom_mails as $a_mail ) {
			$emails[ $a_mail[ 'id' ] ] 	= $a_mail[ 'title' ];
		}

		return $emails;
	}

}

<?php

class WGM_Product {

	public static function init(  ) {
		
		add_filter( 'product_type_options',                                     array( 'WGM_Product', 'register_product_type') );
		add_action( 'woocommerce_variation_options',                            array( 'WGM_Product', 'add_variant_product_type'), 10, 3 );
		add_action( 'woocommerce_update_product_variation',                     array( 'WGM_Product', 'save_variant_product_type'), 10, 1 );
		add_action( 'woocommerce_new_product_variation',                     	array( 'WGM_Product', 'save_variant_product_type'), 10, 1 );
		add_action( 'save_post',                                                array( 'WGM_Product', 'save_product_digital_type'), 10, 2 );
		self::init_product_images();
		self::init_product_attributes();
	}

	/**
	 * Show Product Attributes that are not used for variations in cart, checkout and orders
	 *
	 * @since 3.7
	 * @return void
	 */
	public static function init_product_attributes() {

		if ( get_option( 'gm_show_product_attributes', 'off' ) == 'on' ) {
			add_filter( 'woocommerce_add_cart_item_data', 			array( __CLASS__, 'add_cart_item_data' ),	 		10, 4 );
			add_filter( 'woocommerce_get_item_data', 				array( __CLASS__, 'get_item_data' ),		 		10, 2 );
			add_filter( 'woocommerce_get_cart_item_from_session', 	array( __CLASS__, 'get_cart_item_from_session' ),	10, 3 );
			add_action( 'woocommerce_new_order_item', 				array( __CLASS__, 'new_order_item' ),				10, 3 );
		}

	}

	/**
	 * Add Product Attributes to order Item
	 *
	 * @since 3.7
	 * @wp-hook woocommerce_new_order_item
	 * @param Integer $item_id
	 * @param WC_Order_Item_Product $item
	 * @param Integer $order_id
	 * @return void
	 */
	public static function new_order_item( $item_id, $item, $order_id ) {

		if ( is_a( $item, 'WC_Order_Item_Product' ) ) {

			$product = $item->get_product();

			// Compatibility for plugins that creates a WC_Order_Item_Product without an existing Product
			if ( ! method_exists( $product, 'get_id' ) ) {
				return;
			}

			$data = array();
			$attribute_data = WGM_Product::add_cart_item_data( $data, $product->get_id(), false, false );

			if ( isset( $attribute_data[ 'gm_product_properties' ] ) ) {

				foreach ( $attribute_data[ 'gm_product_properties' ] as $property ) {
					wc_add_order_item_meta( $item_id, $property[ 'name' ], $property[ 'value' ] );
				}

			}

		}

	}

	/**
	 * Add Product Attributes from session to cart item
	 *
	 * @wp-hook woocommerce_get_cart_item_from_session
	 * @since 3.7
	 * @param Array $cart_item_data
	 * @param Array $cart_item_session_data
	 * @param Integer $cart_item_key
	 * @return Array
	 */
	public static function get_cart_item_from_session( $cart_item_data, $cart_item_session_data, $cart_item_key ) {

		if ( isset( $cart_item_session_data[ 'gm_product_properties' ] ) ) {
	        $cart_item_data[ 'gm_product_properties' ] = $cart_item_session_data[ 'gm_product_properties' ];
	    }

		return $cart_item_data;

	}

	/**
	 * Add Product Attributes to cart item data
	 *
	 * @wp-hook woocommerce_add_cart_item_data
	 * @since 3.7
	 * @param Array $cart_item_data
	 * @param Integer $product_id
	 * @param Integer $variation_id
	 * @param Integer $quantity
	 * @return Array
	 */
	public static function add_cart_item_data( $cart_item_data, $product_id, $variation_id, $quantity ) {

		$product = wc_get_product( $product_id );
		
		if ( $product->get_type() == 'variation' ) {
			$product = wc_get_product( $product->get_parent_id() );
		}

		$attributes = $product->get_attributes();

		foreach ( $attributes as $attribute ) {

			if ( ! method_exists( $attribute, 'get_visible' ) ) {
				continue;
			}

			if ( $attribute->get_visible() && ( ! $attribute->get_variation() ) ) {

				$option_names	  = array();

				if ( $attribute->is_taxonomy() ) {
			
					$taxonomy 		= $attribute->get_taxonomy_object();
					$name 			= $taxonomy->attribute_label;
					$option_terms 	= $attribute->get_terms();
					
					foreach ( $option_terms as $term ) {
						$option_names[] = $term->name;
					}
				
				} else {

					$name 			= $attribute->get_name();
					$option_names 	= $attribute->get_options();
					
				}

				$value = implode( ', ', $option_names );

				if ( ! isset( $cart_item_data[ 'gm_product_properties' ] ) ) {
					$cart_item_data[ 'gm_product_properties' ] = array();
				}

				$cart_item_data[ 'gm_product_properties' ][] = array( 
					'name'	=> $name,
					'value'	=> $value,
				);

			}

		}

		return $cart_item_data;
	}

	/**
	 * Get Product Attributes 
	 *
	 * @wp-hook woocommerce_get_item_data
	 * @since 3.7
	 * @param Array $item_data
	 * @param Array $cart_item
	 * @return Array
	 */
	public static function get_item_data ( $item_data, $cart_item ) {

		if ( isset( $cart_item[ 'gm_product_properties' ] ) ) {

			foreach ( $cart_item[ 'gm_product_properties' ] as $property ) {

				$item_data[] = array(
					'name'	=> $property[ 'name' ],
					'value' => $property[ 'value' ],
				); 

			}

		}

		return $item_data;

	}

	/**
	 * Handle Product Images in Cart, Checkout, Orders, Emails
	 *
	 * @since 3.6.4
	 * @return void
	 */
	public static function init_product_images() {

		// Get all options
		$show_images_in = array(
			'cart'		=> get_option( 'german_market_product_images_in_cart', 'on' ),
			'checkout'	=> get_option( 'german_market_product_images_in_checkout', 'off' ),
			'order'		=> get_option( 'german_market_product_images_in_order', 'off' ),
			'email'		=> get_option( 'german_market_product_images_in_emails', 'off' ),
		);

		// Images in Cart
		if ( $show_images_in[ 'cart' ] == 'off' ) {

			add_filter( 'woocommerce_cart_item_thumbnail', '__return_false', 100 );

			// Image is not shown, but table column is still there, let's add some css to hide it
			add_action( 'wp_head', array( __CLASS__, 'hide_thumbnail_column_in_cart_with_css' ) );

		}

		// Images in Checkout
		if ( $show_images_in[ 'checkout' ] == 'on' ) {
			add_filter( 'woocommerce_cart_item_name', array( __CLASS__, 'add_thumbnail_to_checkout' ), 100, 3 );
		}

		// Images In Order
		if ( $show_images_in[ 'order' ] == 'on' ) {

			add_filter( 'woocommerce_order_item_name', array( __CLASS__, 'add_thumbnail_to_order' ), 100, 3 );

			// Add and remove filter in invoice pdfs
			add_action( 'wp_wc_invoice_pdf_start_template', array( __CLASS__, 'remove_add_thumbnail_to_order_invoice_pdfs' ) );
			add_action( 'wp_wc_invoice_pdf_end_template', array( __CLASS__, 'undo_remove_add_thumbnail_to_order_invoice_pdfs' ) );
		}

		// Images in E-Mails
		add_action( 'woocommerce_email_header', array( __CLASS__, 'avoid_double_images_in_emails_header' ), 10, 2 );
		add_action( 'woocommerce_email_footer', array( __CLASS__, 'avoid_double_images_in_emails_footer' ), 10, 1 );
		if ( $show_images_in[ 'email' ] == 'on' ) {
			add_filter( 'woocommerce_email_order_items_args', array( __CLASS__, 'add_thumbnail_to_emails' ), 100, 3 );
		}

	}

	/**
	 * Avoid double images in invoice pdfs: remove filter
	 *
	 * @since 3.7.1
	 * @wp-hook wp_wc_invoice_pdf_start_template
	 * @return void
	 */
	public static function remove_add_thumbnail_to_order_invoice_pdfs() {
		remove_filter( 'woocommerce_order_item_name', array( __CLASS__, 'add_thumbnail_to_order' ), 100, 3 );
	}

	/**
	 * Avoid double images in invoice pdfs: add filter again
	 *
	 * @since 3.7.1
	 * @wp-hook wp_wc_invoice_pdf_end_template
	 * @return void
	 */
	public static function undo_remove_add_thumbnail_to_order_invoice_pdfs() {
		add_filter( 'woocommerce_order_item_name', array( __CLASS__, 'add_thumbnail_to_order' ), 100, 3 );
	}

	/**
	 * Handle Product Images Emails, avoid item image in order item name
	 *
	 * @since 3.6.4
	 * @wp-hook woocommerce_email_header
	 * @param String $email_heading
	 * @param WC_Email
	 * @return void
	 */
	public static function avoid_double_images_in_emails_header( $email_heading, $email = false ) {

		if ( get_option( 'german_market_product_images_in_order', 'off' ) == 'on' ) {
			remove_filter( 'woocommerce_order_item_name', array( __CLASS__, 'add_thumbnail_to_order' ), 100, 3 );
		}

	}

	/**
	 * Handle Product Images Emails, avoid item image in order item name
	 *
	 * @since 3.6.4
	 * @wp-hook woocommerce_email_footer
	 * @param WC_Email
	 * @return void
	 */
	public static function avoid_double_images_in_emails_footer(  $email ) {

		if ( get_option( 'german_market_product_images_in_order', 'off' ) == 'on' ) {
			add_filter( 'woocommerce_order_item_name', array( __CLASS__, 'add_thumbnail_to_order' ), 100, 3 );
		}

	}

	/**
	 * Handle Product Images Emails
	 *
	 * @since 3.6.4
	 * @wp-hook woocommerce_email_order_items_args
	 * @param Array $args
	 * @return Array
	 */
	public static function add_thumbnail_to_emails( $args ) {

		$args[ 'show_image' ] = true;
		return $args;
	}

	/**
	 * Handle Product Images Checkout
	 *
	 * @since 3.6.4
	 * @wp-hook woocommerce_order_item_name
	 * @param String $product_name
	 * @param WC_Order_Item_Product $item
	 * @param Boolean is_visible
	 * @return String
	 */
	public static function add_thumbnail_to_order( $product_name, $item, $is_visible ) {

		if ( ( ! is_checkout() ) && ( ! is_account_page() ) ) {
			return $product_name;
		}

		if ( get_option( 'german_market_product_images_in_cart', 'on' ) == 'off' ) {
			remove_filter( 'woocommerce_cart_item_thumbnail', '__return_false', 100 );
		}

		$_product = $item->get_product();

		if ( $_product && $_product->exists()  ) {

			$product_permalink = apply_filters( 'woocommerce_order_item_permalink', $_product->is_visible() ? $_product->get_permalink( $item ) : '', $item, false );
			$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), false, false );

			ob_start();

				echo '<div class="german-market-product-image order">';

				if ( ! $product_permalink ) {
					echo wp_kses_post( $thumbnail );
				} else {
					printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), wp_kses_post( $thumbnail ) );
				}

				echo '</div>';

			$image = ob_get_clean();

		}

		if ( get_option( 'german_market_product_images_in_cart', 'on' ) == 'off' ) {
			add_filter( 'woocommerce_cart_item_thumbnail', '__return_false', 100 );
		}

		return apply_filters( 'add_thumbnail_to_order', $image . $product_name, $image, $product_name, $item );

	}

	/**
	 * Handle Product Images Checkout
	 *
	 * @since 3.6.4
	 * @wp-hook woocommerce_cart_item_name
	 * @param String $product_name
	 * @param Array $cart_item
	 * @param String cart_item_key
	 * @return String
	 */
	public static function add_thumbnail_to_checkout( $product_name, $cart_item, $cart_item_key ) {

		if ( ! is_checkout() ) {
			return $product_name;
		}

		if ( get_option( 'german_market_product_images_in_cart', 'on' ) == 'off' ) {
			remove_filter( 'woocommerce_cart_item_thumbnail', '__return_false', 100 );
		}

		$_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

		if ( $_product && $_product->exists() && $cart_item['quantity'] ) {

			$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
			$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

			ob_start();

				echo '<div class="german-market-product-image checkout">';

				if ( ! $product_permalink ) {
					echo wp_kses_post( $thumbnail );
				} else {
					printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), wp_kses_post( $thumbnail ) );
				}

				echo '</div>';

			$image = ob_get_clean();

		}

		if ( get_option( 'german_market_product_images_in_cart', 'on' ) == 'off' ) {
			add_filter( 'woocommerce_cart_item_thumbnail', '__return_false', 100 );
		}

		return apply_filters( 'wgm_product_add_thumbnail_to_checkout', $image . $product_name, $image, $product_name, $cart_item, $cart_item_key );

	}

	/**
	 * Add some CSS to hide thumbnail column in cart
	 *
	 * @since 3.6.4
	 * @wp-hook wp_head
	 * @return void
	 */
	public static function hide_thumbnail_column_in_cart_with_css() {

		if ( ! is_cart() ) {
			return;
		}

		if ( apply_filters( 'hide_thumbnail_column_in_cart_with_css', true ) ) {

			?>
			<style>
				table.shop_table.cart th.product-thumbnail, table.shop_table.cart td.product-thumbnail { border: none; width: 0; }
			</style>
			<?php

		}

	}

	/**
	 * @param array $types
	 * @wp-hook product_type_options
	 * @return array $tye
	 */
	public static function register_product_type(array $types){

		$types[ 'digital' ] = array(
				'id'            => '_digital',
				'wrapper_class' => 'show_if_simple',
				'label'         => __( 'Digital', 'woocommerce-german-market' ),
				'description'   => __( 'Only products with this marker will be treated as “digital” in the context of the EU Consumer Rights Directive from June 13, 2014.', 'woocommerce-german-market' ),
				'default'       => 'no'
			);

		return $types;
	}

	/**
	 * @param int $id
	 * @param Post $post
	 * @wp-hook save_post
	 */
	public static function save_product_digital_type( $id, $post ){
		
		if( $post->post_type != 'product' ) {
			return;
		}

		if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE) || ( defined( 'DOING_AJAX' ) && DOING_AJAX) || isset( $_REQUEST[ 'bulk_edit' ] ) ) {
			return;
		}

		if( isset( $_REQUEST[ '_digital' ] ) ) {
			update_post_meta($id, '_digital', 'yes' );
		} else {
			update_post_meta($id, '_digital', 'no' );
		}
	}

	/**
	 * Adds Digital checkbox to variation meta box
	 * @param $loop
	 * @param $variation_data
	 * @param $variation
	 * @wp-hook woocommerce_variation_options
	 */
	public static function add_variant_product_type( $loop, $variation_data, $variation ) {
		$_digital = get_post_meta( $variation->ID, '_digital', true );

		?>
		<label>
			<input type="checkbox" id="_digital" class="checkbox variable_is_digital" name="variable_is_digital[<?php echo $loop; ?>]" <?php checked( isset( $_digital ) ? $_digital : '', 'yes' ); ?> />
				<?php _e( 'Digital', 'woocommerce-german-market' ); ?>
				<a class="tips" data-tip="<?php esc_attr_e( 'Only products with this marker will be treated as “digital” in the context of the EU Consumer Rights Directive from June 13, 2014.', 'woocommerce-german-market' ); ?>" href="#">[?]</a>
			</label>

	<?php
	}

	/**
	 *  Save the digital setting for variations
	 * @param $var_id
	 * @wp-hook woocommerce_update_product_variation
	 * @wp-hook woocommerce_create_product_variation
	 *
	 */
	public static function save_variant_product_type( $var_id ){

		if( ! isset($_POST['variable_post_id'] ) ){
			return;
		}

		$variable_post_id   = $_POST['variable_post_id'];
		$max_loop           = max( array_keys( $_POST['variable_post_id'] ) );

		for ( $i = 0; $i <= $max_loop; $i ++ ) {

			if ( ! isset( $variable_post_id[ $i ] ) ) {
				continue;
			}

			$variable_is_virtual = isset( $_POST['variable_is_digital'] ) ? $_POST['variable_is_digital'] : array();

			$variation_id = absint( $variable_post_id[ $i ] );

			if( $variation_id == $var_id ){
				$is_digital = isset( $variable_is_virtual[ $i ] ) ? 'yes' : 'no';
				update_post_meta( $var_id, '_digital', $is_digital );
			}
		}
	}
}

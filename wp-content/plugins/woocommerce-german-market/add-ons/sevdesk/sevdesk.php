<?php
/* 
 * Add-on Name:	sevDesk
 * Description:	sevDesk API for Woocommerce
 * Version:		1.0
 * Author:		MarketPress
 * Author URI:	http://marketpress.com
 * Licence:		GPLv3
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
} 

if ( ! function_exists( 'sevdesk_woocommerce_init' ) ) {

	/**
	* init
	*
	* @return void
	*/
	function sevdesk_woocommerce_init() {

		// load api
		$backend_dir = untrailingslashit( plugin_dir_path( __FILE__ ) ) . DIRECTORY_SEPARATOR . 'application' . DIRECTORY_SEPARATOR . 'backend';
		require_once( $backend_dir . DIRECTORY_SEPARATOR . 'api.php' );

		if ( is_admin() ) {
			
			// stuff that is only needed in the shop order table
			require_once( $backend_dir . DIRECTORY_SEPARATOR . 'edit-shop-order.php' );
			add_action( 'current_screen', 'sevdesk_woocommerce_edit_shop_order' );

			// settings
			require_once( $backend_dir . DIRECTORY_SEPARATOR . 'settings.php' );
			add_filter( 'woocommerce_de_ui_left_menu_items', 'sevdesk_woocommerce_de_ui_left_menu_items' );

			// ajax handler
			if ( function_exists( 'curl_init' ) ) {

				require_once( $backend_dir . DIRECTORY_SEPARATOR . 'ajax-handler.php' );
				add_action( 'wp_ajax_sevdesk_woocommerce_edit_shop_order', 'sevdesk_woocommerce_edit_shop_order_ajax' );
				add_action( 'wp_ajax_sevdesk_woocommerce_edit_shop_order_refund', 'sevdesk_woocommerce_edit_shop_order_ajax_refund' );
				
			}
				
		}

		// automatic transmission
		require_once( untrailingslashit( plugin_dir_path( __FILE__ ) ) . DIRECTORY_SEPARATOR . 'application' . DIRECTORY_SEPARATOR . 'auto-transmission.php' );

	}
	
	sevdesk_woocommerce_init();

}

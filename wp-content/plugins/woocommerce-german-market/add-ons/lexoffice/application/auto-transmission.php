<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
} 

if ( get_option( 'woocommerce_de_lexoffice_automatic_completed_order', 'off' ) == 'on' ) {
	add_action( 'woocommerce_order_status_completed', 'lexoffice_woocommerce_status_completed', 10, 1 );
}

if ( get_option( 'woocommerce_de_lexoffice_automatic_refund', 'off' ) == 'on' ) {
	add_action( 'woocommerce_create_refund', 'lexoffice_woocommerce_create_refund', 10, 2 );
}

/**
* Send Voucher to lexoffice if order is marked as completed
*
* @since 	GM 3.7.1
* @wp-hook 	woocommerce_order_status_completed
* @param 	Integer $order_id
* @return 	void
*/
function lexoffice_woocommerce_status_completed( $order_id ) {

	$order = wc_get_order( $order_id );
	$response = lexoffice_woocomerce_api_send_voucher( $order );

}

/**
* Send Voucher to lexoffice if refund is created
*
* @since 	GM 3.7.1
* @wp-hook 	woocommerce_create_refund
* @param 	WC_Order_Refund $refund
* @param 	Array $args
* @return 	void
*/
function lexoffice_woocommerce_create_refund( $refund, $args ) {

	$response = lexoffice_woocommerce_api_send_refund( $refund );

}

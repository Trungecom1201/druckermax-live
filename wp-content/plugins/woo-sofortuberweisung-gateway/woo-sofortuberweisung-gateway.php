<?php

/*
 * Plugin Name: Sofortueberweisung Gateway for Woocommerce 
 * Version: 1.2.3
 * Plugin URI: http://www.mlfactory.at/sofortueberweisung-gateway-for-woocommerce
 * Description: Sofortueberweisung Gateway for WooCommerce, easy, fast and safe
 * Author: Michael Leithold
 * Author URI: http://www.mlfactory.at
 * Requires at least: 4.0
 * Tested up to: 5.2
 * License: GPLv2 or later
 * Text Domain: woo-sofortuberweisung-gateway
 *
*/

require_once(dirname(__FILE__).'/sofort/payment/sofortLibSofortueberweisung.inc.php');
require_once(dirname(__FILE__).'/sofort/core/sofortLibNotification.inc.php');
require_once(dirname(__FILE__).'/sofort/core/sofortLibTransactionData.inc.php');

defined( 'ABSPATH' ) or exit;
// Make sure WooCommerce is active
if ( ! in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	return;
}
/**
 * Add the gateway to WC Available Gateways
 * 
 * @since 1.0.0
 * @param array $gateways all available WC gateways
 * @return array $gateways all WC gateways + Sofortüberweisung gateway
 */

 
function sofortueberweisung_gateway_add_to_gateways( $gateways ) {
	$gateways[] = 'WC_Gateway_Sofort';
	return $gateways;
}
add_filter( 'woocommerce_payment_gateways', 'sofortueberweisung_gateway_add_to_gateways' );
/**
 * Adds plugin page links
 * 
 * @since 1.0.0
 * @param array $links all plugin links
 * @return array $links all plugin links + our custom links (i.e., "Settings")
 */
function sofortueberweisung_gateway_plugin_links( $links ) {
	$plugin_links = array(
		'<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout&section=sofortueberweisung_gateway' ) . '">' . __( 'Configure', 'wc-gateway-sofort' ) . '</a>'
	);
	return array_merge( $plugin_links, $links );
}
add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'sofortueberweisung_gateway_plugin_links' );
/**
 * Offline Payment Gateway
 *
 * Provides an Offline Payment Gateway; mainly for testing purposes.
 * We load it later to ensure WC is loaded first since we're extending it.
 *
 * @class 		WC_Gateway_Sofort
 * @extends		WC_Payment_Gateway
 * @version		1.0.0
 * @package		WooCommerce/Classes/Payment
 * @author 		SkyVerge
 */
add_action( 'plugins_loaded', 'sofortueberweisung_gateway_init', 11 );

function sofortueberweisung_gateway_init() {
	class WC_Gateway_Sofort extends WC_Payment_Gateway {
		/**
		 * Constructor for the gateway.
		 */
		public function __construct() {
	  
			$this->id                 = 'sofortueberweisung_gateway';
			$this->icon               = apply_filters('woocommerce_offline_icon', '');
			$this->has_fields         = false;
			$this->method_title       = __( 'Sofortüberweisung', 'wc-gateway-sofort' );
			$this->method_description .= __( 'Allows Sofortüberweisung payments in Woocommerce. Easy, fast and safe. Orders are marked as "processing" when received.', 'wc-gateway-sofort' );
			$this->method_description .= '<div class="notice notice-success" data-nonce="cf716d3210">
			<p>Thanks for using <strong>Sofortüberweisung Gateway</strong>. <br /><br />Please support our free work by rating this plugin with 5 stars on WordPress.org. <a href="https://wordpress.org/support/plugin/sofortueberweisung-gateway-woocommerce/reviews/#new-post" target="_blank">Click here to rate us.</a><br><br><br>
			<b>Information:</b> This plugin work fine in the free version.<br/> In the PRO version of this plugin there are many more Features. <a href="https://wordpress.org/plugins/sofortueberweisung-gateway-woocommerce/"> Check out Features of Pro Version</a></p>
			Buy the Pro Version for only €9,99,- incl. Tax.<br />
			<a href="mailto:michaelleithold18@gmail.com">Send me a email michaelleithold18@gmail.com</a>
			</div>';
		  
			// Load the settings.
			$this->init_form_fields();
			$this->init_settings();
		  
			// Define user set variables
			$this->title        = $this->get_option( 'title' );
			$this->configkey  = $this->get_option( 'configkey' );
			$this->displayService  = $this->get_option( 'displayService' );
			$this->displayOrderStatus  = $this->get_option( 'displayOrderStatus' );
			$this->displayTransID  = $this->get_option( 'displayTransID' );
			$this->displayTransTime  = $this->get_option( 'displayTransTime' );
			$this->displayTransAmount  = $this->get_option( 'displayTransAmount' );
			$this->displayTransInfo  = $this->get_option( 'displayTransInfo' );
			$displayTransInfo1  = $this->get_option( 'displayTransInfo' );
			$cnfgkey  = $this->get_option( 'configkey' );
			$_SESSION["configkeys9384720"] = $this->get_option( 'configkey' );
			$this->status_after_payment  = $this->get_option( 'status_after_payment' );
			$this->description  = $this->get_option( 'description' );
			$this->instructions = $this->get_option( 'instructions' );
			$this->successEmailText = $this->get_option( 'successEmailText' );
		  
			// Actions
			add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
			
			add_action( 'woocommerce_thankyou_' . $this->id, array( $this, 'thankyou_page' ) );
			
			// Customer Emails
			add_action( 'woocommerce_email_before_order_table', array( $this, 'email_instructions' ), 10, 3 );
		}
	
	
		/**
		 * Initialize Gateway Settings Form Fields
		 */
		 
		public function init_form_fields() {
			
	  
			$defaul_email_text = __("
										Wir bedanken uns für Ihren Einkauf und freuen uns Ihnen mitteilen zu können, dass sich Ihre Bestellung nun in Bearbeitung befindet. <br/>
										<br/>
										<u>Details zur Zahlung:</u> <br/>
										Service: Sofortüberweisung<br/>
										Transaktions ID: %transactionID%<br/>
										<br />
										Ihre Bestellung wird umgehend bearbeitet.<br/>
										Sie erhalten gesondert nach dem Versand der Ware eine E-Mail mit der Sendungsnummer.<br/>
										<br/>
										Ihr %sitename% Team			
								");
	  
			$this->form_fields = apply_filters( 'wc_offline_form_fields', array(
		  
				'enabled' => array(
					'title'   => __( 'Enable/Disable Sofortüberweisung', 'wc-gateway-sofort' ),
					'type'    => 'checkbox',
					'label'   => __( 'Enable Sofortüberweisung Payment', 'wc-gateway-sofort' ),
					'default' => 'yes'
					
				),
				
				
				'configkey' => array(
					'title'       => __( 'Configuration key', 'wc-gateway-sofort' ),
					'type'        => 'text',
					'description' => __( 'Get the key from sofort.com. Create a project and click on the "Migrate to Gateway" arrow on the right side of you project on the page "Project overview". Now u see under "Sofort Gateway" a Project. Click on that. Scroll down and you see the "Configuration key for your shop system"', 'wc-gateway-sofort' ),
					'default'     => __( 'xxxxxx:xxxxxx:xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', 'wc-gateway-sofort' ),
					'desc_tip'    => true,
				),
		
				
				'title' => array(
					'title'       => __( 'Title', 'wc-gateway-sofort' ),
					'type'        => 'text',
					'description' => __( 'This controls the title for the payment method the customer sees during checkout.', 'wc-gateway-sofort' ),
					'default'     => __( 'Sofortüberweisung', 'wc-gateway-sofort' ),
					'desc_tip'    => true,
				),
				
				'description' => array(
					'title'       => __( 'Description', 'wc-gateway-sofort' ),
					'type'        => 'textarea',
					'description' => __( 'Payment method description that the customer will see on your checkout.', 'wc-gateway-sofort' ),
					'default'     => __( 'Safe, easy and direct <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/SOFORT_%C3%9CBERWEISUNG_Logo.svg/250px-SOFORT_%C3%9CBERWEISUNG_Logo.svg.png" alt="Sofortüberweisung" style="width:40px">', 'wc-gateway-sofort' ),
					'desc_tip'    => true,
					'readonly' => 'readonly',
				),
			
				'instructions' => array(
					'title'       => __( 'Custom message on order received page', 'wc-gateway-sofort' ),
					'type'        => 'textarea',
					'description' => __( 'Custom Message if order received. Leave it empty if its should not displayed', 'wc-gateway-sofort' ),
					'default'     => 'Order received. Thanks!',
					'desc_tip'    => true,
				),
				
				'successEmailText' => array(
					'title'   => __( 'Success Payment Email Text:', 'wc-gateway-sofort' ),
					'type'    => 'textarea',
					'label'   => __( 'Show or hide "Transaction Time" on Paymentinfo', 'wc-gateway-sofort' ),
					'default' => $defaul_email_text,
					'desc_tip'    => true,
					'description' => __( 'Message Body Payment Success Email. Variables: %sitename% - %transactionID%', 'wc-gateway-sofort' ),					
				),
				
				'displayTransInfo' => array(
					'title'   => __( 'Transaction Info', 'wc-gateway-sofort' ),
					'type'    => 'checkbox',
					'label'   => __( 'Show or hide "Transaction Info" <b>completely</b> on Paymentinfo', 'wc-gateway-sofort' ),
					'default' => 'yes'
				),
				
				'displayService' => array(
					'title'   => __( 'Service', 'wc-gateway-sofort' ),
					'type'    => 'checkbox',
					'label'   => __( 'Show or hide "Service" on Paymentinfo', 'wc-gateway-sofort' ),
					'default' => 'yes'
				),
				
				'displayOrderStatus' => array(
					'title'   => __( 'Order status', 'wc-gateway-sofort' ),
					'type'    => 'checkbox',
					'label'   => __( 'Show or hide "Order Status" on Paymentinfo', 'wc-gateway-sofort' ),
					'default' => 'yes'
				),
				
				'displayTransID' => array(
					'title'   => __( 'Transaction ID', 'wc-gateway-sofort' ),
					'type'    => 'checkbox',
					'label'   => __( 'Show or hide "Transaction ID" on Paymentinfo', 'wc-gateway-sofort' ),
					'default' => 'yes'
				),
				
				'displayTransTime' => array(
					'title'   => __( 'Transaction Time', 'wc-gateway-sofort' ),
					'type'    => 'checkbox',
					'label'   => __( 'Show or hide "Transaction Time" on Paymentinfo', 'wc-gateway-sofort' ),
					'default' => 'yes'
				)
				
			) );
		}
	
		/**
		 * Output for the order received page.
		 */
		public function thankyou_page() {
			if ( $this->instructions ) {
				
				echo wpautop( wptexturize( $this->instructions ) );
			}
		}
	
		/**
		 * Add content to the WC emails.
		 *
		 * @access public
		 * @param WC_Order $order
		 * @param bool $sent_to_admin
		 * @param bool $plain_text
		 */
		public function email_instructions( $order, $sent_to_admin, $plain_text = false ) {
		
			if ( $this->instructions && ! $sent_to_admin && $this->id === $order->payment_method && $order->has_status( 'on-hold' ) ) {
				echo wpautop( wptexturize( $this->instructions ) ) . PHP_EOL;
			}
		}
		
		/**
		 * Process the payment and return the result
		 *
		 * @param int $order_id
		 * @return array
		 */
		public function process_payment( $order_id ) {

	
			$order = new WC_Order( $order_id );
			$shop_name = get_bloginfo( 'name' );	
			$order_key = $order->get_order_key();
			// Start Sofortüberweisung
			
			//get configkey from admin page
			$configkey = $this->get_option( 'configkey' );
			$status_after_payment = $this->get_option( 'status_after_payment' );
			$country = WC()->countries->countries[ $order->get_shipping_country() ];
			if ($country == "Austria" or $country == "Styria") {
				$countrycode = "AT";
			}
			if ($country == "Germany") {
				$countrycode = "DE";
			}	
			if ($country == "Belgium") {
				$countrycode = "BE";
			}	
			if ($country == "Italy") {
				$countrycode = "IT";
			}
			if ($country == "Netherlands") {
				$countrycode = "NL";
			}
			if ($country == "Poland") {
				$countrycode = "PL";
			}	
			if ($country == "Spain") {
				$countrycode = "ES";
			}	
			if ($country == "Switzerland") {
				$countrycode = "CH";
			}				
			
			if (!$countrycode) {
				$countrycode = "DE";
			}
			
			$Sofortueberweisung = new Sofortueberweisung($configkey);
			$Sofortueberweisung->setAmount($order->get_total());
			$Sofortueberweisung->setCurrencyCode("EUR");
			$Sofortueberweisung->setSenderCountryCode($countrycode);
			$Sofortueberweisung->setReason($shop_name.' - Order Nr. '.$order_id, 'Verwendungszweck');
			$Sofortueberweisung->setSuccessUrl($this->get_return_url( $order ).'&transId=-TRANSACTION-', true);
			//$Sofortueberweisung->setAbortUrl($this->get_return_url( $order ).'&transId=-TRANSACTION-&act=failed');$order ->get_cancel_order_url_raw();
			$Sofortueberweisung->setAbortUrl($order ->get_cancel_order_url_raw());
			$Sofortueberweisung->setNotificationUrl(get_site_url());
			$Sofortueberweisung->setCustomerprotection(true);
		
			//send payment request to sofort
			$Sofortueberweisung->sendRequest();

			if($Sofortueberweisung->isError()) {
				//SOFORT-API didn't accept the data
				echo __("<div style='background-color: #eae9e9;border: 1px solid red; padding: 10px;margin-right: 25px;color: #333;margin-left: 25px;'>");
				echo __("<p><b style='color: red;'>SOFORT API FEHLER!</b></p>");
				echo __("<p>Es ist ein Fehler aufgetreten bei der Kommunikation mit der SOFORT API. Bitte prüfen Sie ob der von Ihnen angegebene API Key korrekt und gültig ist!</p>");
				echo __("<p>Sollten Sie nicht der Betreiber des Online Angebots sein wenden Sie sich bitte an den Betreiber.</p>");
				echo __("<p>Fehlercode: ".$Sofortueberweisung->getError()."</p>");
				echo __("</div>");
			} else {
				//buyer must be redirected to $paymentUrl else payment cannot be successfully completed!
				$paymentUrl = $Sofortueberweisung->getPaymentUrl();
				//header('Location: '.$paymentUrl);
				
				//if order success woocommer hook
				// Mark as on-hold (we're awaiting the payment)
				$order->update_status( 'pending', __( 'Awaiting Sofortüberweisung payment', 'wc-gateway-sofort' ) );
				
				// Reduce stock levels
				//$order->reduce_order_stock();
				
				// Remove cart
				//WC()->cart->empty_cart();
				
				$_SESSION["sofort_transactionsid"] = $Sofortueberweisung->getTransactionId();
				
						return array(
							'result' 	=> 'success',
							'redirect'	=> $paymentUrl
							//'redirect'	=> $this->get_return_url( $order )
						);
			}		
		}
  } // end \WC_Gateway_Sofort class
}

class sofortueberweisung_gateway_check_payment {
	
     function __construct() {
		
          add_action('woocommerce_thankyou', 'check_if_paid_sofort', 5, 1);
		  
			function check_if_paid_sofort( $order_id ) {
				
				if ( ! $order_id )
					return;

				// Getting an instance of the order object
				$order = wc_get_order( $order_id );
				
				if($order->is_paid())
					$paid = 'yes';
				else
					$paid = 'no';
				

				if (isset($_GET['transId'])) {
					$transID = $_GET['transId'];
				}

				//sofort response from api
				if (isset($transID)) {

					//get config key from session
					//config key = transaction id
					$configkey = $_SESSION["configkeys9384720"];
					$SofortLibTransactionData = new SofortLibTransactionData($configkey);
					$SofortLibTransactionData->addTransaction($transID);
					$SofortLibTransactionData->setApiVersion('2.0');

					//wait for api / test
					sleep(5);

					//send request to sofort to check transaction
					$SofortLibTransactionData->sendRequest();

					$output = array();
					$methods = array(
								'getAmount' => '',
								'getAmountRefunded' => '',
								'getCount' => '',
								'getPaymentMethod' => '',
								'getConsumerProtection' => '',
								'getStatus' => '',
								'getStatusReason' => '',
								'getStatusModifiedTime' => '',
								'getLanguageCode' => '',
								'getCurrency' => '',
								'getTransaction' => '',
								'getReason' => array(0,0),
								'getUserVariable' => 0,
								'getTime' => '',
								'getProjectId' => '',
								'getRecipientHolder' => '',
								'getRecipientAccountNumber' => '',
								'getRecipientBankCode' => '',
								'getRecipientCountryCode' => '',
								'getRecipientBankName' => '',
								'getRecipientBic' => '',
								'getRecipientIban' => '',
								'getSenderHolder' => '',
								'getSenderAccountNumber' => '',
								'getSenderBankCode' => '',
								'getSenderCountryCode' => '',
								'getSenderBankName' => '',
								'getSenderBic' => '',
								'getSenderIban' => '',
					);
					$status = "";
					$payment_ok = "Payment via Sofortüberweisung successfully! <br /> Transaction ID: $transID <br/>";

					$sofort_options = get_option( 'woocommerce_sofortueberweisung_gateway_settings' );
					
					if (isset($sofort_options['successEmailText'])) {
						$successEmailText = $sofort_options['successEmailText'];
						$successEmailText = str_replace("%transactionID%", $transID, $successEmailText);
						$successEmailText = str_replace("%sitename%", get_bloginfo( 'name' ), $successEmailText);
					} else {
						$successEmailText = "";
					}
					//########################
					// construct email
					//########################

					global $woocommerce;
					// Create a mailer
					$mailer = $woocommerce->mailer();
					$message = $mailer->wrap_message(sprintf( __( 'Zahlungseingang Bestellung %s' ), $order->get_order_number() ), $successEmailText );

					//get counter and check if first time visit
					$sofortTransCounter = get_post_meta( $order_id, 'sofortTransCounter', true );

						foreach($methods as $method => $params) {
							//get transTime time
							if ($method == "getTime") {
							$transTime = $SofortLibTransactionData->$method();	
							}
							if ($method == "getAmount") {
							$amount = $SofortLibTransactionData->$method();	
							}
							if ($method == "getCurrency") {
							$currency = $SofortLibTransactionData->$method();	
							}
							error_reporting(0);
							if($params && count($params) == 2) {
								if ($method == "getStatus" && $sofortTransCounter != 1) {
									if ($SofortLibTransactionData->$method($params[0], $params[1])) {
										$status .= "ok";
										$order->update_status('processing', 'order_note');
										$order->add_order_note($payment_ok,0,true);
										// SEND MAIL
										add_post_meta( $order_id, 'sofortTransCounter', 1 );
										$mailer->send( $order->billing_email, sprintf( __( 'Zahlungseingang Bestellung %s' ), $order->get_order_number() ), $message );
										$order->reduce_order_stock();
										WC()->cart->empty_cart();
									} else {
										$order->update_status('on-hold', 'order_note');
										$order->add_order_note("Payment via Sofortüberweisung <b>NOT</b> successfully!",0,true);
										$status .= "notok";
									}
								}
								$output[] = $method . ': ' . $SofortLibTransactionData->$method($params[0], $params[1]);
							} else if($params !== '') {
								if ($method == "getStatus" && $sofortTransCounter != 1) {
									if ($SofortLibTransactionData->$method($params)) {
										$status .= "ok";
										$order->update_status('processing', 'order_note');
										$order->add_order_note($payment_ok,0,true);
										// SEND MAIL
										add_post_meta( $order_id, 'sofortTransCounter', 1 );
										$mailer->send( $order->billing_email, sprintf( __( 'Zahlungseingang Bestellung %s' ), $order->get_order_number() ), $message );
										$order->reduce_order_stock();
										WC()->cart->empty_cart();
									} else {
										$order->update_status('on-hold', 'order_note');
										$order->add_order_note("Payment via Sofortüberweisung <b>NOT</b> successfully!",0,true);
										$status .= "notok";
									}
								}
								$output[] = $method . ': ' . $SofortLibTransactionData->$method($params);
							} else {
								$output[] = $method . ': ' . $SofortLibTransactionData->$method();
								if ($method == "getStatus" && $sofortTransCounter != 1) {
									if ($SofortLibTransactionData->$method()) {
										$status .= "ok";
										$order->update_status('processing', 'order_note');
										$order->add_order_note($payment_ok,0,true);
										// SEND MAIL
										add_post_meta( $order_id, 'sofortTransCounter', 1 );
										$mailer->send( $order->billing_email, sprintf( __( 'Zahlungseingang Bestellung %s' ), $order->get_order_number() ), $message );
										$order->reduce_order_stock();
										WC()->cart->empty_cart();
									} else {
										$order->update_status('on-hold', 'order_note');
										$order->add_order_note("Payment via Sofortüberweisung <b>NOT</b> successfully!",0,true);
										$status .= "notok";
									}
								}
							}
						}
						
					$res ="";
					if($SofortLibTransactionData->isError()) {
						$res.= $SofortLibTransactionData->getError();	
					} else {
						$res.= implode('<br />', $output);
							
						
							
						//get all datas from backend
						$settings = get_option( 'woocommerce_sofortueberweisung_gateway_settings' );
						$displayService = $settings['displayService'];
						$displayOrderStatus = $settings['displayOrderStatus'];
						$displayTransID = $settings['displayTransID'];
						$displayTransTime = $settings['displayTransTime'];
						//$displayTransAmount = $settings['displayTransAmount'];
						$displayTransInfo = $settings['displayTransInfo'];

						//display or not?
						if ($displayService == "yes") { $displayService = "style=''"; } else { $displayService = "style='display: none;'"; }
						if ($displayOrderStatus == "yes") { $displayOrderStatus = "style=''"; } else { $displayOrderStatus = "style='display: none;'"; }
						if ($displayTransID == "yes") { $displayTransID = "style=''"; } else { $displayTransID = "style='display: none;'"; }
						if ($displayTransTime == "yes") { $displayTransTime = "style=''"; } else { $displayTransTime = "style='display: none;'"; }
						if ($displayTransInfo == "yes" or $displayTransInfo == "") { $displayTransInfo = "style=''"; } else { $displayTransInfo = "style='display: none;'"; }

						// Displaying transaction details
						echo '	<div class="col-xs" '.$displayTransInfo.'>
								<div class="card" style="padding: 20px;">
								<h2 class="woocommerce-order-details__title">Payment status</h2>
								<table class="woocommerce-table woocommerce-table--customer-details shop_table customer_details">
								<tbody>
									<tr>
									<tr '.$displayService.'><th>'.__( 'Service').':</th> <td>Sofortüberweisung</td></tr>
									<tr '.$displayOrderStatus.'><th>'.__( 'Order status').':</th> <td>' . $order->get_status() . '</td></tr>
									<tr '.$displayTransID.'><th>'.__( 'Transaction ID').':</th> <td>' . $transID . '</td></tr>			
							';
						//if payment failed status pending
						if (isset($_GET['act']) == "failed" or $order->get_status() == "pending") {
						$cart_url = get_permalink( wc_get_page_id( 'cart' ) );
						echo '	<tr><th>Start payment now:</th><td><a href="'.$cart_url.'/order-pay/'.$order_id.'/?pay_for_order=true&amp;key='.$order->order_key.'" class="woocommerce-button button pay">Pay</a></td></tr>';	
						} else {
						echo "	<tr $displayTransTime><th>".__( 'Transaction time').":</th> <td>$transTime</td></tr>";
						}
						
						echo '	</tbody>
								</table>	
								</div>
								</div>
						';
					}
				}
			}
	}
}

new sofortueberweisung_gateway_check_payment();

?>
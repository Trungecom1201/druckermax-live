=== Sofortueberweisung Gateway for Woocommerce ===
Contributors: mlfactory
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=5GUU55ZDMKSEA
Tags: payment gateway, sofortueberweisung, klarna, woocommerce
Requires at least: 4
Tested up to: 5.2
Requires PHP: 5.2.4
Stable tag: 1.2.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Allows your users to pay over Sofort&uuml;berweisung via Woocommerce checkout.
Easy, fast and safe.
Setup is very easy.

== Description ==
= Englisch =
Allows your users to pay over Sofort&uuml;berweisung via Woocommerce checkout.
Easy, fast and safe.
Setup is very easy.

Features PRO Version
*   comming soon

If you like to buy the Pro Version of this plugin please contact me michaelleithold18@gmail.com

= German/Deutsch =
Erlaubt deinen Kunden per Sofort&uuml;berweisung zu bezahlen &uuml;ber Woocommerce.
Super einfach, schnell und sicher.
Sehr einfache Einrichtung.

**Requires Woocommerce 3+**

Features PRO Version
* Customize easy all texts via the backend
*   comming soon

Wollen Sie die Pro Version des Plugin kaufen bitte kontaktieren Sie mich unter michaelleithold18@gmail.com

    
== Installation ==
1. Upload plugin over wordpress
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Configure the Plugin (Woocommerce->Settings->Checkout->Sofortueberweisung)
4. Get your config key from sofort.com and enter the key in the config key field. For testing u can use this config key here 170609:444875:eb01ca7b51dcc18c9595d373a0c3409c
This API is in Demo Mode.
So u can test the plugin with this config key.
Data to use for bank account: choose Austria as country, bank code 00000, user id 000000 and pin 000000, select bank account, tan 00000. 
 
== Frequently Asked Questions ==
 
= comming soon =
 
comming soon
 
 
== Screenshots ==
1. Backend view
2. Backend view payments
3. Backend view sofortueberweisung settings
4. Frontend view order received page
5. Frontend view checkout apge select payment method
 
== Changelog ==
 
= 1.0.3 =
* released stable version
= 1.1 =
* API Fix / Security Fix Post/Get Request - IMPORTANT UPDATE!
= 1.1 =
* Payment Language same as in Woocommerce set / fixed bug if order canceled / fixed some spelling mistake
= 1.2.1 =
* Payment country depend on order country - added customize email text via settings - added error message if sofort api key is not correct - fix notice transid if payment not sofortueberweisung - fix WC Country Notice
= 1.2.2 =
* Payment country depend on order country - added customize email text via settings - added error message if sofort api key is not correct - fix notice transid if payment not sofortueberweisung - fix WC Country Notice
= 1.2.3 =
* Fixed Bug - if payment success empty cart




